//
// Created by Madleina Caduff on 19.10.18.
//

#ifndef TDATA_H
#define TDATA_H

#include "BangolinTypes.h"
#include "coretools/Storage/TDataFile.h"
#include "genometools/GenomePositions/TDistances.h"
#include "coretools/Main/TParameters.h"
#include "genometools/VCF/TPopulationLikelihoods.h"
#include "stattools/ParametersObservations/TValue.h"

//--------------------------------------------
// TEnvironmentalDataReader
//--------------------------------------------

class TEnvironmentalDataReader {
protected:
	size_t _numIndividuals = 0;
	size_t _numEnvVar      = 0;

	void _fillTransposedObservationFromFile(coretools::TMultiDimensionalStorage<TypeUX, 2> &DT);
	void _transposeData(coretools::TMultiDimensionalStorage<TypeUX, 2> &DT,
	                    coretools::TMultiDimensionalStorage<TypeUX, 2> &Data) const;
	void _normalizeToUnitLength(coretools::TMultiDimensionalStorage<TypeUX, 2> &Data) const;

public:
	// read data
	void readFileAndFillObservation(coretools::TMultiDimensionalStorage<TypeUX, 2> &Data);

	// get individual names
	size_t getNumEnvVar() const;
	size_t getNumIndividuals() const;
};

namespace Data {
size_t readData(coretools::TMultiDimensionalStorage<TypeUX, 2> &X, coretools::TMultiDimensionalStorage<TypeGTL, 2> &GTL,
                genometools::TDistancesBinned<numDistGroupsType> *Distances, size_t NumLatFac);
} // namespace Data

#endif // TDATA_H
