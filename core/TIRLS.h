//
// Created by madleina on 11.05.22.
//

#ifndef BANGOLIN_TIRLS_H
#define BANGOLIN_TIRLS_H

#include "TLogisticLookup.h"
#include <armadillo>

// implements the Iteratively reweighted least squares (IRLS) algorithm
// see Murphy (http://noiselab.ucsd.edu/ECE228/Murphy_Machine_Learning.pdf), page 251
namespace IRLS {
arma::vec doIRLS(const arma::vec &y, const arma::mat &X, const arma::mat &Xt, const arma::vec &InitialW,
                 double Epsilon = 0.0001, size_t MaxIter = 100);
arma::vec doIRLS(const arma::vec &y, const arma::mat &X, const arma::mat &Xt, double Epsilon = 0.0001,
                 size_t MaxIter = 100);
} // namespace IRLS

#endif // BANGOLIN_TIRLS_H
