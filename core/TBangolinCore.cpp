//
// Created by madleina on 28.07.21.
//

#include "TBangolinCore.h"
#include "coretools/Math/TProbabilityDistributions.h"

using namespace coretools::instances;
using namespace stattools;
using namespace stattools::prior;

//----------------------------------
// TBangolinModelUVX
//----------------------------------

TBangolinModelUVX::TBangolinModelUVX(const std::string &Filename, stattools::TDAGBuilder &DAGBuilder,
                                     coretools::TMultiDimensionalStorage<TypeUX, 2> &StorageX)
    // constructor for inference
    : _u("u", std::make_shared<stattools::prior::TUniformFixed<TParameterBase, TypeUX, 2>>(), {Filename}),
      _v("v", std::make_shared<stattools::prior::TUniformFixed<TParameterBase, TypeUX, 2>>(), {Filename}),
      _x("x", std::make_shared<stattools::prior::TUniformFixed<TObservationBase, TypeUX, 2>>(), StorageX, {Filename}) {

	_u.getDefinition().setJumpSizeForAll(false);
	_v.getDefinition().setJumpSizeForAll(false);
	_u.getDefinition().setEqualNumberOfUpdates(false);

	_addToDAG(DAGBuilder);
}

TBangolinModelUVX::TBangolinModelUVX(const std::string &Filename, stattools::TDAGBuilder &DAGBuilder,
                                     coretools::TMultiDimensionalStorage<TypeUX, 2> &StorageX, bool)
    // constructor for simulation
    : _u("u", std::make_shared<stattools::prior::TNormalFixed<TParameterBase, TypeUX, 2>>(),
         _createDefForSimulation<TParameterDefinition>("U", Filename)),
      _v("v", std::make_shared<stattools::prior::TNormalFixed<TParameterBase, TypeUX, 2>>(),
         _createDefForSimulation<TParameterDefinition>("V", Filename)),
      _x("x", std::make_shared<stattools::prior::TNormalFixed<TObservationBase, TypeUX, 2>>(), StorageX,
         _createDefForSimulation<TParameterDefinition>("X", Filename)) {

	_addToDAG(DAGBuilder);
}

void TBangolinModelUVX::_addToDAG(stattools::TDAGBuilder &DAGBuilder) {
	DAGBuilder.addToDAG({&_u, &_v});
	DAGBuilder.addToDAG(&_x);
}

//----------------------------------
// TBangolinModelWZ
//----------------------------------

std::string readPriorsOnGamma() {
	double relativePeakWidth = parameters().getParameterWithDefault("relativePeakWidth", 10.);
	if (relativePeakWidth < 1.0) {
		UERROR("Parameter 'relativePeakWidth' must be >= 1.0 (not ", relativePeakWidth, ")!");
	}
	// peak width under Bernouilli = 1/(1-pi)
	// peak width under HMM = (1+gamma)/(1-pi)
	// -> gamma = relativePeakWidth - 1
	// -> exponential prior -> mean = 1/Lambda -> Lambda = 1/mean -> Lambda = 1/(relativePeakWidth-1)
	double expectedGamma = relativePeakWidth - 1.;
	if (expectedGamma == 0.0) {
		// make sure that lambda != 0 (invalid for exponential prior)
		expectedGamma = 10e-10;
	}
	double lambda = 1. / expectedGamma;
	logfile().list("Will use lambda = ", lambda,
	               " as parameter on the exponential prior on gamma (results in a expected peak width that is ",
	               relativePeakWidth, " larger than expected under Bernouilli model).");
	return coretools::str::toString(lambda);
}

void TBangolinModelWZ::_initZ(const std::string &Filename, const std::string &PriorParamsPi,
                              const std::string &PriorParamsRhos, size_t D,
                              genometools::TDistancesBinnedBase *Distances) {
	// with linkage
	_priorOnPi = std::make_shared<TBetaFixed<TParameterBase, TypePi, 1>>();
	_pi        = std::make_unique<stattools::TParameterTyped<TypePi, 1>>("pi", _priorOnPi,
                                                                  TParameterDefinition(Filename, PriorParamsPi));
	_gamma     = std::make_unique<stattools::TParameterTyped<TypeGamma, 1>>(
        "gamma", std::make_shared<TExponentialFixed<TParameterBase, TypeGamma, 1>>(),
        TParameterDefinition(Filename, readPriorsOnGamma()));
	_rhos = std::make_unique<stattools::TParameterTyped<TypeRhos, 1>>(
	    "rhos", std::make_shared<prior::TDirichletFixed<TParameterBase, TypeRhos, 1>>(),
	    TParameterDefinition(Filename, PriorParamsRhos));
	_z = std::make_unique<stattools::TParameterTyped<TypeZ, 1>>(
	    "z",
	    std::make_shared<THMMCategoricalInferred<TParameterBase, TypeZ, 1, TypePi, TypeGamma, TypeRhos>>(
	        _pi.get(), _gamma.get(), _rhos.get(), D, Distances),
	    TParameterDefinition(Filename + "_z"));
}

void TBangolinModelWZ::_initZ(const std::string &Filename, const std::string &PriorParamsRhos, size_t D) {
	// without linkage
	_rhos = std::make_unique<stattools::TParameterTyped<TypeRhos, 1>>(
	    "rhos", std::make_shared<prior::TDirichletFixed<TParameterBase, TypeRhos, 1>>(),
	    TParameterDefinition(Filename, PriorParamsRhos));
	_z = std::make_unique<stattools::TParameterTyped<TypeZ, 1>>(
	    "z", std::make_shared<prior::TCategoricalInferred<TParameterBase, TypeZ, 1, TypeRhos>>(_rhos.get(), D + 1),
	    TParameterDefinition(Filename + "_z"));
}

void TBangolinModelWZ::_initW(const std::string &Filename, bool UniformPriorW) {
	if (UniformPriorW) {
		_w = std::make_unique<stattools::TParameterTyped<TypeVW, 1>>(
		    "w", std::make_shared<TUniformFixed<TParameterBase, TypeVW, 1>>(), TParameterDefinition(Filename));
	} else {
		_muW = std::make_unique<stattools::TParameterTyped<TypeMuW, 1>>(
		    "mu_w", std::make_shared<TNormalFixed<TParameterBase, TypeMuW, 1>>(),
		    TParameterDefinition(Filename, "0,1"));
		_varW0 = std::make_unique<stattools::TParameterTyped<TypeVarsW, 1>>(
		    "varW_0", std::make_shared<TExponentialFixed<TParameterBase, TypeVarsW, 1>>(),
		    TParameterDefinition(Filename, "1"));
		_varW1 = std::make_unique<stattools::TParameterTyped<TypeVarsW, 1>>(
		    "varW_1", std::make_shared<TExponentialFixed<TParameterBase, TypeVarsW, 1>>(),
		    TParameterDefinition(Filename, "1"));
		_w = std::make_unique<stattools::TParameterTyped<TypeVW, 1>>(
		    "w",
		    std::make_shared<TTwoNormalsMixedModelInferred<TParameterBase, TypeVW, 1, TypeMuW, TypeVarsW, TypeZ>>(
		        _z.get(), _muW.get(), _varW0.get(), _varW1.get(), false),
		    TParameterDefinition(Filename));
	}
}

TBangolinModelWZ::TBangolinModelWZ(const std::string &Filename, stattools::TDAGBuilder &DAGBuilder, bool UniformPriorW,
                                   const std::string &PriorParamsPi, const std::string &PriorParamsRhos, size_t D,
                                   genometools::TDistancesBinnedBase *Distances) {
	// constructor with linkage
	_initZ(Filename, PriorParamsPi, PriorParamsRhos, D, Distances);
	_initW(Filename, UniformPriorW);
	_addToDAG(Filename, true, UniformPriorW, D, DAGBuilder);
}

TBangolinModelWZ::TBangolinModelWZ(const std::string &Filename, stattools::TDAGBuilder &DAGBuilder, bool UniformPriorW,
                                   const std::string &PriorParamsRhos, size_t D) {
	// constructor without linkage
	_initZ(Filename, PriorParamsRhos, D);
	_initW(Filename, UniformPriorW);
	_addToDAG(Filename, false, UniformPriorW, D, DAGBuilder);
}

void TBangolinModelWZ::_addToDAG(const std::string &Filename, bool Linkage, bool UniformPriorW, size_t D,
                                 stattools::TDAGBuilder &DAGBuilder) {
	if (Linkage) { _gamma->getDefinition().setPropKernel(ProposalKernel::MCMCProposalKernel::scaleLogNormal); }
	if (!UniformPriorW) {
		_muW->getDefinition().setInitVal("0.0");
		_muW->getDefinition().update(false); // don't update _muW
	}
	_z->getDefinition().setStatePosteriorsFile(Filename + "_z");
	_z->getDefinition().setPropKernel(ProposalKernel::randomInteger);
	_w->getDefinition().setJumpSizeForAll(false);

	// set max
	TypeZ::setMax(D);

	if (Linkage) { DAGBuilder.addToDAG({_pi.get(), _gamma.get()}); }
	DAGBuilder.addToDAG({_rhos.get(), _z.get()});

	if (!UniformPriorW) { DAGBuilder.addToDAG({_muW.get(), _varW0.get(), _varW1.get()}); }
	DAGBuilder.addToDAG(_w.get());
}

//----------------------------------
// TBangolinModelF
//----------------------------------

TBangolinModelF::TBangolinModelF(const std::string &Filename, stattools::TDAGBuilder &DAGBuilder, bool Simulate)
    : _logAlphaF("log_alpha_f", std::make_shared<TUniformFixed<TParameterBase, TypeLogAlphaF, 1>>(), {Filename}),
      _logBetaF("log_beta_f", std::make_shared<TUniformFixed<TParameterBase, TypeLogBetaF, 1>>(), {Filename}),
      _f("f",
         std::make_shared<TBetaInferred<TParameterBase, TypeF, 1, TypeLogAlphaF, TypeLogBetaF, true, true>>(&_logAlphaF,
                                                                                                            &_logBetaF),
         {Filename}) {

	if (Simulate) {
		// when simulating: draw random value from exponential distribution
		// (if value is drawn from uniform distribution, we get crazy large values)
		_logAlphaF.getDefinition().setInitVal(coretools::str::toString(randomGenerator().getExponentialRandom(1.)));
		_logBetaF.getDefinition().setInitVal(coretools::str::toString(randomGenerator().getExponentialRandom(1.)));
	} else {
		_f.getDefinition().setJumpSizeForAll(false);
	}

	DAGBuilder.addToDAG({&_logAlphaF, &_logBetaF, &_f});
}

//----------------------------------
// Free helper functions
//----------------------------------

std::string readPriorsOnPi() {
	// options for beta prior on pi:
	// 1) user specifies mean and variance of pi; calculate alphaPi and betaPi that yield this mean and variance
	std::string nameExpectedPi = "expectedPi";
	std::string nameVarPi      = "varPi";
	coretools::StrictlyPositive<double> alpha, beta;
	if (parameters().parameterExists(nameExpectedPi) && parameters().parameterExists(nameVarPi)) {
		auto expectedPi = parameters().getParameter<double>(nameExpectedPi);
		auto varPi      = parameters().getParameter<double>(nameVarPi);

		std::tie(alpha, beta) = coretools::probdist::TBetaDistr::calculateAlphaBetaForGivenMeanVar(expectedPi, varPi);
	}
	// 2) user specifies mean of pi; set alphaPi = 1 (--> mode is at 0) and calculate betaPi that yields this mean
	// 3) user specifies nothing -> set alphaPi = 1 and calculate betaPi such that a mean of 0.01 results
	else {
		double expectedPi = parameters().getParameterWithDefault(nameExpectedPi, 0.01);
		alpha             = 1.;
		beta              = 1. / expectedPi - 1.;
	}
	logfile().list("Will use alpha = ", alpha, " and beta = ", beta, " as parameters on the beta prior on pi.");
	return coretools::str::toString(alpha) + "," + coretools::str::toString(beta);
}

std::string repeatAlphaRho(coretools::StrictlyPositive<double> AlphaRho, size_t D, bool Linkage) {
	if (Linkage) { return coretools::str::repeat(AlphaRho, D); }
	return coretools::str::repeat(AlphaRho, D + 1);
}

std::string readAlphaRho(size_t D, bool Linkage) {
	// user has provided alpha_rho on command line
	std::vector<coretools::StrictlyPositive<double>> alpha_rhos;
	parameters().fillParameterIntoContainer("alpha_rho", alpha_rhos, ',');

	if (alpha_rhos.empty()) { UERROR("Invalid argument 'alpha_rho': is empty!"); }
	if (alpha_rhos.size() == 1) {
		return repeatAlphaRho(alpha_rhos[0], D, Linkage); // expand vector to match size
	}
	if ((alpha_rhos.size() == D && Linkage) || (alpha_rhos.size() == D + 1 && !Linkage)) { // size matches!
		return coretools::str::concatenateString(alpha_rhos, ",");
	}
	// else throw
	std::string size = (Linkage) ? coretools::str::toString(D) : coretools::str::toString(D + 1);
	UERROR("Size of argument 'alpha_rho' (", alpha_rhos.size(), ") does not match expected size (", size, ").");
}

std::string readPriorsOnRho(size_t D, bool Linkage) {
	// dirichlet prior on rho:
	std::string alpha_rho;
	if (parameters().parameterExists("alpha_rho")) {
		alpha_rho = readAlphaRho(D, Linkage);
	} else {
		alpha_rho = repeatAlphaRho(1.0, D, Linkage); // uniform
	}
	logfile().list("Will use alpha_rho = ", alpha_rho, " as parameters on the Dirichlet prior on rho.");
	return alpha_rho;
}

size_t readMaxDistance_LociHMM() {
	// maximum distance for binning distances in HMM
	size_t maxDistanceLoci = 1000000; // results in 20 distance groups --> todo: ok?
	if (parameters().parameterExists("maxDistanceLoci")) {
		maxDistanceLoci = parameters().getParameter<size_t>("maxDistanceLoci");
		logfile().list("Argument 'maxDistanceLoci': Will consider a maximum distance of " +
		               coretools::str::toString(maxDistanceLoci) + " between loci for the HMM.");
	}
	return maxDistanceLoci;
}

//----------------------------------
// TBangolinCore
//----------------------------------

void TBangolinCore::_parseCommandLineArguments() {
	// read command line arguments
	_filename = parameters().getParameterWithDefault<std::string>("out", "bangolin");
}

void TBangolinCore::runBangolin(bool Simulate) {
	_parseCommandLineArguments();

	// read data
	genometools::TDistancesBinned<numDistGroupsType> distances(readMaxDistance_LociHMM());
	coretools::TMultiDimensionalStorage<TypeUX, 2> storageX;
	coretools::TMultiDimensionalStorage<TypeGTL, 2> storageGTL;
	size_t D = _readData(storageX, storageGTL, distances);

	// build f
	TDAGBuilder dagBuilder;
	TBangolinModelF modelF(_filename, dagBuilder, Simulate);

	// build z
	auto priorParamsOnPi  = readPriorsOnPi();
	bool linkage          = parameters().parameterExists("linkage");
	bool uniformPriorW    = parameters().parameterExists("uniformPriorW");
	auto priorParamsOnRho = readPriorsOnRho(D, linkage);
	TBangolinModelWZ modelWZ(linkage ? TBangolinModelWZ(_filename, dagBuilder, uniformPriorW, priorParamsOnPi,
	                                                    priorParamsOnRho, D, &distances)
	                                 : TBangolinModelWZ(_filename, dagBuilder, uniformPriorW, priorParamsOnRho, D));

	_run(dagBuilder, storageX, storageGTL, modelF, modelWZ);
}

//----------------------------------
// TBangolin_Inference
//----------------------------------

void TBangolin_Inference::_parseCommandLineArguments() {
	TBangolinCore::_parseCommandLineArguments();

	_K = parameters().getParameter<size_t>("numLatFac"); // no default value
	logfile().list("Will use K = ", _K, " latent factors to model population structure.");
}

size_t TBangolin_Inference::_readData(coretools::TMultiDimensionalStorage<TypeUX, 2> &X,
                                      coretools::TMultiDimensionalStorage<TypeGTL, 2> &GTL,
                                      genometools::TDistancesBinned<numDistGroupsType> &Distances) {
	return Data::readData(X, GTL, &Distances, _K);
}

void TBangolin_Inference::_run(stattools::TDAGBuilder &DAGBuilder, coretools::TMultiDimensionalStorage<TypeUX, 2> &X,
                               coretools::TMultiDimensionalStorage<TypeGTL, 2> &GTL, TBangolinModelF &ModelF,
                               TBangolinModelWZ &ModelWZ) {
	// build model u, v, x
	TBangolinModelUVX modelUVX(_filename, DAGBuilder, X);

	// create GTL
	auto prior = std::make_shared<TBangolinPrior>(ModelF.f(), modelUVX.x(), modelUVX.u(), modelUVX.v(), ModelWZ.w(),
	                                              ModelWZ.z(), _K, _filename);
	TObservationTyped<TypeGTL, 2> GTLs("genotypeLikelihoods", prior, GTL, {});
	DAGBuilder.addToDAG(&GTLs);
	DAGBuilder.addExtraUpdater(prior->getPtrToUpdaterMuVZ()); // add updater for mu-v-z joint update

	// run MCMC
	DAGBuilder.buildDAG();
	TMCMC mcmc;
	mcmc.runMCMC(_filename, &DAGBuilder);
}

//----------------------------------
// TBangolin_Simulator
//----------------------------------

void TBangolin_Simulator::_run(stattools::TDAGBuilder &DAGBuilder, coretools::TMultiDimensionalStorage<TypeUX, 2> &X,
                               coretools::TMultiDimensionalStorage<TypeGTL, 2> &GTL, TBangolinModelF &ModelF,
                               TBangolinModelWZ &ModelWZ) {
	// build model u, v, x
	TBangolinModelUVX modelUVX(_filename, DAGBuilder, X, true);

	// create GTL
	auto prior = std::make_shared<TBangolinPrior>(ModelF.f(), modelUVX.x(), modelUVX.u(), modelUVX.v(), ModelWZ.w(),
	                                              ModelWZ.z(), _K, _filename);
	TObservationTyped<TypeGTL, 2> GTLs("genotypeLikelihoods", prior, GTL, {});
	DAGBuilder.addToDAG(&GTLs);

	// simulate
	DAGBuilder.buildDAG();
	TSimulator simulator;
	simulator.simulate(_filename, &DAGBuilder);
}

size_t TBangolin_Simulator::_readData(coretools::TMultiDimensionalStorage<TypeUX, 2> &X,
                                      coretools::TMultiDimensionalStorage<TypeGTL, 2> &GTL,
                                      genometools::TDistancesBinned<numDistGroupsType> &Distances) {
	// read numEnvVar
	_D = parameters().getParameterWithDefault("numEnvVar", 1);
	logfile().list("Number of environmental variables: ", _D);

	// resize
	X.resize(std::array<size_t, 2>{_D, _n});
	GTL.resize(std::array<size_t, 2>{_L, _n});

	// simulate dimension names
	_simulateNames(X, GTL, Distances);

	return _D;
}

size_t calculateNumIndividuals_forSimulating(size_t NumSamples_1, double Depth_1, double Depth_2) {
	size_t numBpFlowCell = 2.7e+12; // = 2700 * pow(10, 9); source: Daniel, NovaSeq S4
	size_t costFlowCell  = 30000;   // source: Daniel, NovaSeq S4, in dollar
	size_t genomeSize    = 3.1e+09; // 3.1 * pow(10, 9);  example mammal genome (bp)
	size_t costLibrary   = 10;      // 1 library per sample

	double costFlowCellPerBp = (double)costFlowCell / (double)numBpFlowCell;
	// 1 library costs 1/3 as 1x depth
	double a                 = (double)costLibrary / ((double)costFlowCellPerBp * (double)genomeSize);
	return std::floor(((double)NumSamples_1 * (Depth_1 + a)) / (Depth_2 + a));
}

void TBangolin_Simulator::_parseCommandLineArguments() {
	TBangolinCore::_parseCommandLineArguments();

	logfile().startIndent("Reading parameters for simulation:");
	// loci
	_L = parameters().getParameterWithDefault("numLoci", 10000);
	logfile().list("Number of loci: ", _L);

	// number of latent factors
	_K = parameters().getParameterWithDefault<size_t>("numLatFac", 2);
	logfile().list("Will use K = ", _K, " latent factors to model population structure.");

	// get budget and mean depth and compute number of individuals that can be afforded based on this
	size_t budgetNumIndividuals = parameters().getParameterWithDefault("budgetNumIndividuals", 100);
	double budgetMeanDepth      = parameters().getParameterWithDefault("budgetMeanDepth", 20.);
	logfile().list("Will consider a fixed budget for sequencing ", budgetNumIndividuals, " individuals at ",
	               budgetMeanDepth, "x.");

	double meanDepth = parameters().getParameterWithDefault("meanDepth", 20);
	_n               = calculateNumIndividuals_forSimulating(budgetNumIndividuals, budgetMeanDepth, meanDepth);
	logfile().list("Will simulate ", _n, " individuals with a mean depth of ", meanDepth, "x.");

	logfile().endIndent();
}

void TBangolin_Simulator::_simulateNames(coretools::TMultiDimensionalStorage<TypeUX, 2> &X,
                                         coretools::TMultiDimensionalStorage<TypeGTL, 2> &GTL,
                                         genometools::TDistancesBinned<numDistGroupsType> &Distances) {
	// "simulate" names: sample, loci and envVar names
	_simulateSampleNames(X, GTL);
	_simulateEnvNames(X);
	_simulateDistances(Distances);
	GTL.setDimensionName(std::make_shared<genometools::TNamesPositions>(&Distances), 0);
}

void TBangolin_Simulator::_simulateSampleNames(coretools::TMultiDimensionalStorage<TypeUX, 2> &X,
                                               coretools::TMultiDimensionalStorage<TypeGTL, 2> &GTL) const {
	auto sampleNames = std::make_shared<coretools::TNamesStrings>();
	for (size_t i = 0; i < _n; i++) { sampleNames->addName({"Sample" + coretools::str::toString(i + 1)}); }

	// add to x and data
	X.setDimensionName(sampleNames, 1);
	GTL.setDimensionName(sampleNames, 1);
}

void TBangolin_Simulator::_simulateEnvNames(coretools::TMultiDimensionalStorage<TypeUX, 2> &X) const {
	auto envNames = std::make_shared<coretools::TNamesStrings>();
	for (size_t i = 0; i < _D; i++) { envNames->addName({"Env" + coretools::str::toString(i + 1)}); }

	// add to x
	X.setDimensionName(envNames, 0);
}

void TBangolin_Simulator::_simulateDistances(genometools::TDistancesBinned<numDistGroupsType> &Distances) {
	// junks
	std::vector<size_t> junks = {_L};
	if (parameters().parameterExists("junks")) {
		parameters().fillParameterIntoContainer("junks", junks, ',');
		logfile().list("Junks: ", coretools::str::concatenateString(junks));

		// check if total size of positions matches number of loci
		auto totalSize = std::accumulate(std::begin(junks), std::end(junks), 1, std::multiplies<>());
		if ((size_t)totalSize != _L) {
			UERROR("Failed to simulate: Argument 'numLoci' (", _L,
			       ") differs from number of loci given by argument 'junks': ", totalSize, "!");
		}
	}

	// Poisson rate lambda for simulating distances
	double lambda = 10;
	if (parameters().parameterExists("rateDistances")) {
		lambda = parameters().getParameter<double>("rateDistances");
		logfile().list("Poisson rate for distances: ", lambda, ".");
	}
	bool simulateWithFixedDistance = false;
	size_t fixedDistance           = 0;
	if (parameters().parameterExists("fixedDistances")) {
		simulateWithFixedDistance = true;
		fixedDistance             = parameters().getParameter<size_t>("fixedDistances");
		logfile().list("Will simulate with a fixed distance of ", fixedDistance, ".");
	}

	Distances.simulate(junks, lambda, simulateWithFixedDistance, fixedDistance);
}