//
// Created by madleina on 11.05.22.
//

#include "TIRLS.h"

namespace IRLS {

void _fillSZ(size_t N, const arma::vec &y, const arma::mat &X, const arma::vec &w, arma::vec &S, arma::vec &z) {
	arma::mat nu = X * w;
	for (size_t i = 0; i < N; i++) {
		double mu = TLogisticLookup::approxLogistic(nu[i]);
		S[i]      = mu * (1. - mu);
		if (S[i] == 0.0) { S[i] = 0.000001; } // prevent division by zero
		z[i] = nu[i] + (y[i] - mu) / S[i];
	}
}

arma::vec _OLS(size_t K, size_t N, const arma::mat &Xt, const arma::vec &S, const arma::mat &X, const arma::vec &z) {
	arma::mat XtSX(K, K, arma::fill::zeros);
	arma::vec XtSz(K, arma::fill::zeros);
	for (size_t k = 0; k < K; k++) {
		for (size_t i = 0; i < N; i++) {
			double XtS_k_i = Xt.at(k, i) * S.at(i, i);
			XtSz[k] += XtS_k_i * z[i];
			for (size_t j = 0; j < K; j++) { XtSX.at(k, j) += XtS_k_i * X.at(i, j); }
		}
	}
	return arma::solve(XtSX, XtSz);
}

arma::vec doIRLS(const arma::vec &y, const arma::mat &X, const arma::mat &Xt, const arma::vec &InitialW, double Epsilon,
                 size_t MaxIter) {
	// extract dimensions
	const auto N = y.size();
	assert(N == X.n_rows);
	const auto K = X.n_cols;
	assert(K == InitialW.size());

	// prepare coefficients that should be estimated
	arma::vec w = InitialW;
	// run!
	arma::vec S(N);
	arma::vec z(N);
	for (size_t iter = 0; iter < MaxIter; iter++) {
		_fillSZ(N, y, X, w, S, z);
		arma::vec newW = _OLS(K, N, Xt, S, X, z);

		// check for convergence
		double sumOfSquares = 0.0;
		for (size_t k = 0; k < K; k++) { sumOfSquares += (newW[k] - w[k]) * (newW[k] - w[k]); }
		if (sumOfSquares < Epsilon) { return newW; }
		w = newW;
	}
	return w;
}

arma::vec doIRLS(const arma::vec &y, const arma::mat &X, const arma::mat &Xt, double Epsilon, size_t MaxIter) {
	// overloaded function that starts with initial w=0
	arma::vec initialW = arma::vec(X.n_cols, arma::fill::zeros);
	return doIRLS(y, X, Xt, initialW, Epsilon, MaxIter);
}

} // namespace IRLS
