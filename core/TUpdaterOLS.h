//
// Created by madleina on 25.09.22.
//

#ifndef BANGOLIN_TUPDATEROLS_H
#define BANGOLIN_TUPDATEROLS_H

#include "stattools/Updates/TUpdate.h"

class TUpdaterOLS : public stattools::TUpdateBase {
private:
	std::string _name;
	bool _isUpdated = true;

	std::vector<double> _sumOfLogs;
	std::vector<size_t> _total;
	std::vector<size_t> _counterLargerZero;

	std::vector<double> _proposalWidth;

	const double _idealProposalRatio = 1.0;

	std::string _prefix;

protected:
	std::pair<size_t, size_t> _get90QuantilesBinomial(size_t NumUpdates) const {
		using namespace coretools::probdist;
		size_t k_lower = TBinomialDistr::invCumulativeDensity(0.05, NumUpdates, 0.5);
		size_t k_upper = TBinomialDistr::invCumulativeDensity(0.95, NumUpdates, 0.5);
		return {k_lower, k_upper};
	}

	bool _isWithin90QuantileBinomial(size_t NumUpdates, size_t CounterLargerOne) const {
		// idea: if proposal width is good tuned, then approx. 1/2 of all proposal ratios should be > 1 and 1/2 should
		// be < 1 --> binomial distribution
		const auto [k_lower, k_upper] = _get90QuantilesBinomial(NumUpdates);
		return CounterLargerOne >= k_lower && CounterLargerOne <= k_upper;
	}

	void _writePropWidthsToFile() const {
		coretools::TOutputFile file(_prefix + "_proposalWidthsOLS.txt");
		file.writeln(_proposalWidth);
		file.close();
	}

public:
	void initialize(size_t Size, bool IsUpdated, const std::string &Name, const std::string &Prefix) {
		_name      = Name;
		_isUpdated = IsUpdated;
		_prefix    = Prefix;

		_sumOfLogs.resize(Size, 0.0);
		_total.resize(Size, 0);
		_counterLargerZero.resize(Size, 0);
		_proposalWidth.resize(Size, 1.0);

		// read proposal width from file?
		if (coretools::instances::parameters().parameterExists("proposalWidthsOLS")) {
			std::string filename = coretools::instances::parameters().getParameterFilename("proposalWidthsOLS");
			coretools::TInputFile file(filename, coretools::TFile_Filetype::fixed);
			std::vector<double> tmp;
			while (file.read(tmp)) {};
			if (tmp.size() != Size) {
				UERROR("Size of proposal widths in file '", filename, "' does not match expected size ", Size, "!");
			}
			_proposalWidth = tmp;
		}
	}

	void addLogProposalRatio(size_t Index, double LogRatio) {
		if (_isUpdated) {
			_sumOfLogs[Index] += LogRatio;
			_total[Index]++;
			if (LogRatio > 0.0) { _counterLargerZero[Index]++; }
		}
	}

	double propose(double Value, size_t Index) {
		if (_isUpdated) {
			return coretools::instances::randomGenerator().getNormalRandom(Value, _proposalWidth[Index]);
		}
		return Value;
	}

	double logHastingsRatioPropKernel(size_t Index, double Sampled, double OLS_Mean, double OldSampled,
	                                  double Old_OLS_Mean) const {
		using namespace coretools::probdist;

		if (_isUpdated) {
			return TNormalDistr::logDensity(OldSampled, Old_OLS_Mean, _proposalWidth[Index]) -
			       TNormalDistr::logDensity(Sampled, OLS_Mean, _proposalWidth[Index]);
		}
		return 0.0;
	}

	double logProposalRatio(size_t Index) const {
		// Note: we calculate the mean of the log(proposalRatio)
		// -> not biased with large values
		// -> e.g. mean(c(0.1, 10)) = 5.05; whereas exp(mean(c(log(0.1), log(10)))) = 1
		if (_total[Index] > 0) { return _sumOfLogs[Index] / (double)_total[Index]; }
		return 0.0;
	}

	double proposalRatio(size_t Index) const { return exp(logProposalRatio(Index)); }

	double meanProposalRatio() const {
		coretools::TMeanVar<double> log_meanVar;
		for (size_t i = 0; i < _sumOfLogs.size(); i++) {
			if (_total[i] > 0) { log_meanVar.add(logProposalRatio(i)); }
		}
		return exp(log_meanVar.mean());
	}

	void adjustProposalWidth() override {
		for (size_t i = 0; i < size(); i++) {
			if (_isUpdated && _total[i] > 0) {
				double scale = _idealProposalRatio / proposalRatio(i);

				// cap to prevent too small/too large scaling
				scale = std::max(scale, 0.5);
				scale = std::min(scale, 2.0);

				_proposalWidth[i] *= scale;
			}
		}
	};

	void printAccRateToLogfile() const override {
		using namespace coretools::instances;
		logfile().conclude("Mean proposal ratio ", name() + " = ", meanProposalRatio());

		// write proposal widths at end of each burnin & MCMC
		_writePropWidthsToFile();
	};

	void clear() override {
		std::fill(_sumOfLogs.begin(), _sumOfLogs.end(), 0.0);
		std::fill(_total.begin(), _total.end(), 0.0);
		std::fill(_counterLargerZero.begin(), _counterLargerZero.end(), 0);
	}

	std::pair<size_t, size_t> numAccRatesWithin90PQuantileOfBinomial() const override {
		// for each update kernel: check if the number of proposal ratios lies within the 90% quantiles of a
		// binomial distribution for a proposal ratio = 1.0
		// return pair: first number is the number of kernels that are within quantile;
		// second number if the total number of kernels
		if (!hasAcceptanceRate()) { return {0, 0}; }
		size_t c = 0;
		for (size_t i = 0; i < this->size(); i++) {
			c += _isWithin90QuantileBinomial(_total[i], _counterLargerZero[i]);
		}
		return {c, this->size()};
	};

	bool proposalWidthIsShared() const override { return false; };
	coretools::Probability acceptanceRate(size_t) const override { return 0.0; };
	size_t size() const override { return _sumOfLogs.size(); };
	bool hasAcceptanceRate() const override { return _isUpdated && coretools::containerSum(_total) > 0; };
	const std::string &name() const override { return _name; };
};

#endif // BANGOLIN_TUPDATEROLS_H
