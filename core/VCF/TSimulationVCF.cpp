/*
 * TSimulationVCF.cpp
 *
 *  Created on: Feb 28, 2018
 *      Author: phaentu
 */

#include "TSimulationVCF.h"

//-------------------------------
// TVCFWriter
//-------------------------------

TVCFWriter::TVCFWriter(const std::shared_ptr<coretools::TNamesEmpty> &PositionNames,
                       const std::shared_ptr<coretools::TNamesEmpty> &SampleNames) {
	_positionNames = PositionNames;
	_sampleNames   = SampleNames;

	_vcfOpen    = false;
	_locusIndex = 0;

	// set genotype strings
	_truePLStrings[0] = "0,100,100";
	_truePLStrings[1] = "100,0,100";
	_truePLStrings[2] = "100,100,0";
}

TVCFWriter::~TVCFWriter() { closeVcf(); }

void TVCFWriter::openVCF(const std::string &Filename) {
	// open file
	_filename = Filename;
	_vcf.open(_filename.c_str());
	if (!_vcf) { DEVERROR("Failed to open file '", _filename, "'!"); }
	_vcfOpen = true;
}

void TVCFWriter::closeVcf() {
	if (_vcfOpen) {
		_vcf << "\n";
		_vcf.close();
		_vcfOpen = false;
	}
}

void TVCFWriter::writeHeader() {
	// write info
	_vcf << "##fileformat=VCFv4.2\n";
	_vcf << "##source=Simulation\n";
	_vcf << "##FORMAT=<ID=GT,Number=1,Type=String,Description=\"Genotype\">\n";
	_vcf << "##FORMAT=<ID=GQ,Number=1,Type=Integer,Description=\"Genotype Quality\">\n";
	_vcf << "##FORMAT=<ID=PL,Number=G,Type=Integer,Description=\"Phred-scaled genotype likelihoods\">\n";
	_vcf << "##FORMAT=<ID=DP,Number=G,Type=Integer,Description=\"Depth at site\">\n";

	// write header
	_vcf << "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT";
	for (size_t i = 0; i < _sampleNames->size(); i++) { _vcf << "\t" << (*_sampleNames)[i]; }
}

void TVCFWriter::newSite() {
	std::vector<std::string> junk_pos = _positionNames->getName(_locusIndex);
	assert(junk_pos.size() == 2);

	_vcf << '\n' << junk_pos[0] << '\t' << junk_pos[1] << "\t.\tA\tC\t50\t.\t.\tGT:GQ:PL:DP";
	_locusIndex++;
}

void TVCFWriter::writeTrueGenotype(genometools::BiallelicGenotype Geno) {
	_vcf << "\t" << toString(Geno) << ":30:";
	_vcf << _truePLStrings[altAlleleCounts(Geno)];
	_vcf << ":" << 100;
}

void TVCFWriter::_writeMissingCell() {
	_vcf << "\t"
	     << "./."
	     << ":.:";
	_vcf << "0,0,0";
}
