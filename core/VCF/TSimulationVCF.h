/*
 * TSimulationVCF.h
 *
 *  Created on: Feb 28, 2018
 *      Author: phaentu
 */

#ifndef TSIMULATIONVCF_H_
#define TSIMULATIONVCF_H_

#include "coretools/Main//TLog.h"
#include "coretools/Storage/TNames.h"
#include "stattools/ParametersObservations/TObservationTyped.h"
#include "genometools/TSampleLikelihoods.h"
#include "coretools/Files/gzstream.h"
#include "coretools/Strings/stringFunctions.h"
#include <stdexcept>
#include <vector>

//-------------------------------
// TVCFWriter
//-------------------------------

class TVCFWriter {
private:
	// about vcf
	std::string _filename;
	gz::ogzstream _vcf;
	bool _vcfOpen;

	// common strings
	std::string _truePLStrings[3];

	// names
	std::shared_ptr<coretools::TNamesEmpty> _positionNames;
	std::shared_ptr<coretools::TNamesEmpty> _sampleNames;

	// keep track of loci
	size_t _locusIndex;

	void _writeMissingCell();
	template<typename GenotypeLikelihoodType>
	void _writeCell(const genometools::TSampleLikelihoods<GenotypeLikelihoodType> &SampleLikelihoods) {
		// genotype with lowest phred-score is the observed genotype
		genometools::BiallelicGenotype observedGenotype   = SampleLikelihoods.mostLikelyGenotype();
		genometools::BiallelicGenotype secondBestGenotype = SampleLikelihoods.secondMostLikelyGenotype();

		// write to vcf
		_vcf << "\t" << toString(observedGenotype) << ":" << SampleLikelihoods[secondBestGenotype] << ":";
		_vcf << coretools::str::toString(SampleLikelihoods[genometools::BiallelicGenotype::homoFirst]) + "," +
		            coretools::str::toString(SampleLikelihoods[genometools::BiallelicGenotype::het]) + "," +
		            coretools::str::toString(SampleLikelihoods[genometools::BiallelicGenotype::homoSecond]);
	};

public:
	TVCFWriter(const std::shared_ptr<coretools::TNamesEmpty> &PositionNames,
	           const std::shared_ptr<coretools::TNamesEmpty> &SampleNames);
	~TVCFWriter();

	// open and close
	void openVCF(const std::string &Filename);
	void closeVcf();

	// header
	void writeHeader();

	// rownames
	void newSite();

	// write
	template<typename GenotypeLikelihoodType>
	void write(const genometools::TSampleLikelihoods<GenotypeLikelihoodType> &SampleLikelihoods, size_t Depth) {
		if (Depth == 0) { // missing genotypes
			_writeMissingCell();
		} else {
			_writeCell(SampleLikelihoods);
		}
		_vcf << ":" << Depth;
	};
	void writeTrueGenotype(genometools::BiallelicGenotype Geno);
};

#endif /* TSIMULATIONVCF_H_ */
