//
// Created by madleina on 22.03.21.
//

#ifndef BANGOLIN_BANGOLINTYPES_H
#define BANGOLIN_BANGOLINTYPES_H

#include "genometools/PhredProbabilityTypes.h"
#include "genometools/TSampleLikelihoods.h"
#include "coretools/Types/commonWeakTypes.h"
#include "coretools/Types/probability.h"

// create Weak type for z:
// gets as interval WeakTypeIntervalPositiveMaxVariable (because max of z is variable)
class WeakTypeZ
    : public coretools::UnsignedIntBase<uint8_t, WeakTypeZ, coretools::WeakTypeIntervalPositiveMaxVariable, WeakTypeZ> {
public:
	WeakTypeZ()
	    : coretools::UnsignedIntBase<uint8_t, WeakTypeZ, coretools::WeakTypeIntervalPositiveMaxVariable, WeakTypeZ>(
	          0){};
	constexpr WeakTypeZ(uint8_t Value)
	    : coretools::UnsignedIntBase<uint8_t, WeakTypeZ, coretools::WeakTypeIntervalPositiveMaxVariable, WeakTypeZ>(
	          Value){}; // not explicit
	constexpr explicit WeakTypeZ(coretools::tags::NoCheck, std::string_view Value)
	    : coretools::UnsignedIntBase<uint8_t, WeakTypeZ, coretools::WeakTypeIntervalPositiveMaxVariable, WeakTypeZ>(
	          coretools::tags::NoCheck{}, Value){};
	constexpr explicit WeakTypeZ(std::string_view Value)
	    : coretools::UnsignedIntBase<uint8_t, WeakTypeZ, coretools::WeakTypeIntervalPositiveMaxVariable, WeakTypeZ>(
	          Value){};
};

typedef uint8_t numDistGroupsType;

typedef coretools::WeakType<double> TypeLogAlphaF;
typedef coretools::WeakType<double> TypeLogBetaF;
typedef coretools::ZeroOneOpen<double> TypeF;

typedef coretools::Positive<double> TypeGamma;
typedef coretools::ZeroOneOpen<double> TypePi;
typedef coretools::ZeroOpenOneClosed<double> TypeRhos;
typedef WeakTypeZ TypeZ;

typedef coretools::WeakType<double> TypeUX; // type for u and x must be the same

typedef coretools::WeakType<double> TypeVW;
typedef coretools::WeakType<double> TypeMuW;
typedef coretools::StrictlyPositive<double> TypeVarsW;

typedef genometools::TSampleLikelihoods<genometools::PhredIntProbability> TypeGTL;

#endif // BANGOLIN_BANGOLINTYPES_H
