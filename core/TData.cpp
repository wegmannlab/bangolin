//
// Created by Madleina Caduff on 19.10.18.
//

#include "TData.h"

using namespace coretools::instances;

//--------------------------------------------
// TEnvironmentalDataReader
//--------------------------------------------

void TEnvironmentalDataReader::readFileAndFillObservation(coretools::TMultiDimensionalStorage<TypeUX, 2> &Data) {
	// read file
	coretools::TMultiDimensionalStorage<TypeUX, 2> transposed;
	_fillTransposedObservationFromFile(transposed);

	// fill into storage
	_transposeData(Data, transposed);
	_normalizeToUnitLength(Data);
}

void checkForDuplicateNames(const std::shared_ptr<coretools::TNamesEmpty> &IndivNames,
                            const std::shared_ptr<coretools::TNamesEmpty> &EnvNames) {
	// check for duplicates
	auto dupEnv = coretools::findDuplicate(*EnvNames);
	if (dupEnv.first) {
		UERROR("Error while reading envFile: Environmental variable ", (*EnvNames)[dupEnv.second],
		       " occurs twice! Please remove duplicate column.");
	}
	auto dupIndiv = coretools::findDuplicate(*IndivNames);
	if (dupIndiv.first) {
		UERROR("Error while reading envFile: Individual ", (*IndivNames)[dupIndiv.second],
		       " occurs twice! Please remove duplicate row.");
	}
}

void TEnvironmentalDataReader::_fillTransposedObservationFromFile(coretools::TMultiDimensionalStorage<TypeUX, 2> &DT) {
	std::string filename = parameters().getParameterFilename("env");
	logfile().startIndent("Reading environmental measurements from file '" + filename + "'.");

	// prepare names
	auto individualNames = std::make_shared<coretools::TNamesStrings>();
	auto envVarNames     = std::make_shared<coretools::TNamesStrings>();

	// read file
	coretools::TDataReaderBase<TypeUX, 2> reader;
	size_t guessNumIndividuals = 100; // doesn't matter, not too many anyways
	reader.read(filename, &DT, {individualNames, envVarNames}, {'\n', '\t'}, {true, true}, {0, 0}, {false, false},
	            guessNumIndividuals, coretools::colNames_multiline);

	_numEnvVar      = envVarNames->size();
	_numIndividuals = individualNames->size();
	checkForDuplicateNames(individualNames, envVarNames);
	logfile().endIndent("Detected a total number of ", _numEnvVar, " environmental variables and ", _numIndividuals,
	                    " individuals in file ", filename, ".");
}

void TEnvironmentalDataReader::_transposeData(coretools::TMultiDimensionalStorage<TypeUX, 2> &Data,
                                                            coretools::TMultiDimensionalStorage<TypeUX, 2> &DT) const {
	// fill data into observation (transpose)
	Data.prepareFillData(_numEnvVar, {_numIndividuals});
	for (size_t d = 0; d < _numEnvVar; d++) {
		for (size_t i = 0; i < _numIndividuals; i++) {
			auto value = DT[DT.getIndex({i, d})];
			Data.emplace_back(value); // fill as linearized D times n matrix
		}
	}
	Data.finalizeFillData();

	// add names
	Data.setDimensionName(DT.getDimensionName(1), 0);
	Data.setDimensionName(DT.getDimensionName(0), 1);
}

void TEnvironmentalDataReader::_normalizeToUnitLength(coretools::TMultiDimensionalStorage<TypeUX, 2> &Data) const {
	// standardize each environmental variable to have unit length
	for (size_t d = 0; d < _numEnvVar; d++) {
		double sum = 0.0;
		for (size_t i = 0; i < _numIndividuals; i++) {
			const size_t index = Data.getIndex({d, i});
			sum += Data[index] * Data[index];
		}

		double sqrt_sum = sqrt(sum);
		for (size_t i = 0; i < _numIndividuals; i++) { Data[Data.getIndex({d, i})] /= sqrt_sum; }
	}
}

size_t TEnvironmentalDataReader::getNumEnvVar() const { return _numEnvVar; }

size_t TEnvironmentalDataReader::getNumIndividuals() const { return _numIndividuals; }

//--------------------------------------------
// TDataReader
//--------------------------------------------
namespace Data {

auto createSamples(size_t NumIndividuals, std::shared_ptr<coretools::TNamesEmpty> &SampleNamesEnv) {
	std::vector<std::string> samplesEnv(NumIndividuals);
	for (size_t i = 0; i < NumIndividuals; i++) { samplesEnv[i] = (*SampleNamesEnv)[i]; }
	return genometools::TPopulationSamples(samplesEnv);
}

size_t readData(coretools::TMultiDimensionalStorage<TypeUX, 2> &X, coretools::TMultiDimensionalStorage<TypeGTL, 2> &GTL,
              genometools::TDistancesBinned<numDistGroupsType> *Distances, size_t NumLatFac) {
	// first read file with environmental variables
	TEnvironmentalDataReader envReader;
	envReader.readFileAndFillObservation(X);
	size_t numEnvVar        = envReader.getNumEnvVar();
	auto individualNamesEnv = X.getDimensionName(1);
	size_t numIndividuals   = individualNamesEnv->size();

	// create samples object from individual names of env-file
	auto samples = createSamples(numIndividuals, individualNamesEnv);

	// read VCF
	typedef stattools::TValueFixed<TypeGTL> ValueGTL;
	genometools::TPopulationLikelihoods<ValueGTL, genometools::TDistancesBinned<numDistGroupsType>> likelihoods(
	    Distances);
	likelihoods.setSamples(samples);
	likelihoods.doSaveAlleleFrequencies();
	likelihoods.readData();

	// set storage
	GTL = likelihoods.getStorage();

	// finally: check if enough data to estimate parameters
	if ((size_t)(NumLatFac + numEnvVar + 1) > numIndividuals) {
		UERROR("There are more degrees of freedom (", NumLatFac + numEnvVar + 1, ") than individuals (", numIndividuals,
		       ")! Increase the number of individuals or decrease K or D.");
	}

	return numEnvVar;
}

} // namespace Data
