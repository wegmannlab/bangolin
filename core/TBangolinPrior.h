//
// Created by madleina on 16.03.21.
//

#ifndef BANGOLIN_TBANGOLINPRIOR_H
#define BANGOLIN_TBANGOLINPRIOR_H

#include <armadillo>

#include "BangolinTypes.h"
#include "TGramSchmidt.h"
#include "TIRLS.h"
#include "TLogisticLookup.h"
#include "TUpdaterOLS.h"
#include "VCF/TSimulationVCF.h"
#include "coretools/Main/TParameters.h"
#include "coretools/Math/TLogZeroOneLookup.h"
#include "coretools/Math/TProbabilityDistributions.h"
#include "coretools/Math/TSumLog.h"
#include "coretools/Storage/TDataFile.h"
#include "coretools/algorithms.h"
#include "stattools/ParametersObservations/TIndexSampler.h"
#include "stattools/ParametersObservations/TObservationTyped.h"
#include "stattools/ParametersObservations/TParameterTyped.h"
#include "stattools/Updates/TUpdateWeights.h"

#ifdef _OPENMP
#include "omp.h"
#endif

//-------------------------------------------
// TBangolinPrior
//-------------------------------------------

class TBangolinPrior : public stattools::prior::TBaseLikelihoodPrior<stattools::TObservationBase, TypeGTL, 2>,
                       public stattools::TLatentVariable<double, size_t, size_t> {
protected:
	using typename TBase<stattools::TObservationBase, TypeGTL, 2>::Storage;

	// bangolin parameters
	stattools::TParameterTyped<TypeF, 1> *_f;
	stattools::TObservationTyped<TypeUX, 2> *_x;
	stattools::TParameterTyped<TypeUX, 2, true> *_u;
	stattools::TParameterTyped<TypeVW, 2> *_v;
	stattools::TParameterTyped<TypeVW, 1> *_w;
	stattools::TParameterTyped<TypeZ, 1> *_z;

	// main dimensions
	size_t _D = 0;
	size_t _K = 0;
	size_t _L = 0;
	size_t _n = 0;

	// files
	std::string _prefix;

	// helpers / temporary variables
	coretools::TMultiDimensionalStorage<double, 2> _logit_f_li;
	size_t _counterIterationsLogitFli     = 0;
	size_t _numIterUntilResettingLogitFli = 10000;

	std::vector<double> _mus;
	std::vector<double> _data_fli;
	std::vector<arma::mat> _curXUWithIntercept;
	std::vector<arma::mat> _curtXUWithIntercept;
	arma::mat _curUWithIntercept;
	arma::mat _curtUWithIntercept;

	// extra proposal kernel for z, w, v and f
	bool _updateZRegularly = false;
	TUpdaterOLS _updaterOLS;
	stattools::TUpdateWeights<1> _updateWeightsMuWZ_Even;
	stattools::TUpdateWeights<1> _updateWeightsMuWZ_Odd;
	bool _calculateUpdateWeights = true;

	// OMP
	size_t _maxNumThreads = 1;

	// check if dimensions of all parameters and x are compatible
	void _extractDimensionsFromData();
	void _extractDimensionsFromGTLs();

	// common functions
	double _calculateLogit_f_li(size_t l, size_t i) const;
	double _calculateLogit_f_li(size_t l, size_t i, size_t Z) const;
	double _calculateLogit_f_li_minus_mu_l(size_t l, size_t i) const;
	double _calculateLogit_f_li_minus_mu_l(size_t l, size_t i, size_t Z) const;
	void _setLogitFli();
	void _addToCounterReSetLogitFli();
	void _initializeLogitFli();
	coretools::Probability _calculateLikelihood(const Storage *Data, size_t l, size_t i,
	                                            coretools::Probability f_li) const;
	coretools::LogProbability _calculateSumOverIndividuals(size_t l, const Storage *Data) const;
	double _sumVlUi_2_K(size_t i, size_t l) const;
	double _sumVlUi_ZeroModel(size_t i, size_t l) const;
	double _sumVlUi_NonZeroModel(size_t i, size_t l, size_t Z) const;
	void _fillDataFli(const Storage *Data);

	// common functions for initialization
	void _standardizeLogitFli(arma::vec &Logit_f_li) const;
	void _fillLogitFli(arma::vec &f_li, size_t l) const;
	void _initializeTemporaryVariables();
	// initialize mu
	void _setInitialF(size_t l, double Mu);
	// initialize u
	void _estimateInitialU();
	double _normalizeUk(const coretools::TRange &Range);
	std::vector<double> _normalizeAllU();
	void _updateCurArmadilloMatUAndXU();
	arma::mat _fillArmadilloX() const;
	arma::mat _fillArmadilloU2_UK() const;
	arma::mat _fillArmadilloXUWithIntercept(size_t d) const;
	arma::mat _fillArmadilloUWithIntercept() const;
	arma::mat _calculateVectorProjection(const arma::mat &Mat) const;
	void _calculateInitialU2_UK();
	void _calculateInitialU1();
	void _projectMatrixOutOfLogitFli(arma::vec &Logit_f_li, const arma::mat &Projection) const;
	arma::mat _calculateEigenvectors_PCA_U(bool projectX, bool projectU2_UK) const;
	arma::mat _calculateSForInitializationU(bool projectX, bool projectU2_UK) const;
	void _setInitialMuAndVAndZ(size_t l, const std::vector<arma::vec> &Mu_w_v_OLS, const Storage *Data,
	                           std::vector<double> &LLRatios);
	void _readInitialUpdateWeights(const std::string &Filename);
	void _setInitialUpdateWeights(stattools::UpdatingScheme::MCMCUpdatingScheme UpdatingScheme);
	void _setUpdateWeightsMuVWZ(const std::vector<double> &P_not_neutral);
	void _setUpdateWeightsMuVWZ_Parallel(size_t Start, const std::vector<double> &P_not_neutral,
	                                     stattools::TUpdateWeights<1> &UpdateWeights, const std::string &Out);
	std::vector<double> _calculatePNotNeutral() const;
	void _addToExyAndEx(const arma::vec &logit_f_li, arma::mat &Exy, arma::mat &Ex) const;
	arma::mat _calculateS(const arma::mat &Exy, const arma::vec &Ex) const;
	// initialize update weights, mu, beta and v
	void _estimateInitialMuAndVAndZ(const Storage *Data);
	void _setInitialVW(const arma::vec &MuV, size_t l);
	void _setInitialZ(size_t value, size_t l);

	// update one f_l
	void _updateLoci();
	double _calcLLUpdateF_l(size_t l, double &NewMu, double &DiffNewOld, coretools::LogProbability &NewSum,
	                        coretools::LogProbability OldSum, const Storage *Data) const;
	double _calcLogHUpdateF_l(size_t l, double &NewMu, double &DiffNewOld, coretools::LogProbability &NewSum,
	                          coretools::LogProbability OldSum, const Storage *Data) const;
	void _updateF_l(size_t l, coretools::LogProbability &OldSum, const Storage *Data);

	// pairwise update of u
	double _calculateLikRatioU(size_t i, size_t l, size_t Linear, double V_lk, const Storage *Data) const;
	double _calcHastingsRatioUpdateU_k(size_t Locus, const coretools::TRange &IxParameters) const;
	double _calcDiffNewOldUpdateU(size_t Linear, double V_lk) const;
	bool _locusIsRelevantForUpdateU(size_t k, size_t l) const;
	void _updateU_k(size_t i, size_t j, size_t k);
	void _updateU();

	// update one v_kl
	size_t _getIndexV(size_t l, size_t k) const;
	double _calcLLUpdateV_lk(size_t LinearIndex, size_t l, size_t k, double *DiffNewOld,
	                         coretools::LogProbability &NewSum, coretools::LogProbability OldSum,
	                         const Storage *Data) const;
	double _calcLogHUpdateV_lk(size_t LinearIndex, size_t l, size_t k, double *DiffNewOld,
	                           coretools::LogProbability &NewSum, coretools::LogProbability OldSum,
	                           const Storage *Data) const;
	void _updateV_lk(size_t LinearIndex, size_t l, size_t k, coretools::LogProbability &OldSum, const Storage *Data);
	void _updateVW(size_t l, coretools::LogProbability &OldSum, const Storage *Data);

	// update w
	size_t _getIndexX(size_t l, size_t i) const;
	size_t _getIndexXForZ(size_t Z, size_t i) const;
	double _sumUpdateWU(size_t l, double *DiffNewOld, const Storage *Data) const;
	double _sumUpdateWX(size_t l, double *DiffNewOld, const Storage *Data) const;
	double _calcLLUpdateW_l(size_t l, double *DiffNewOld, coretools::LogProbability &NewSum,
	                        coretools::LogProbability OldSum, const Storage *Data) const;
	double _calcLogHUpdateW_l(size_t l, double *DiffNewOld, coretools::LogProbability &NewSum,
	                          coretools::LogProbability OldSum, const Storage *Data) const;
	void _updateW_l(size_t l, coretools::LogProbability &OldSum, const Storage *Data);

	// update z (regularly, no OLS)
	void _updateZ(size_t l, coretools::LogProbability &OldSum, const Storage *Data);
	void _updateAllZRegularly();
	void _updateAllZOLS();
	void _updateAllZOLS_Indices(bool Even, const std::vector<size_t> &Indices);
	std::pair<bool, std::vector<size_t>> _getIndicesZUpdate(stattools::TUpdateWeights<1> &UpdateWeights);
	void _updateAllZOLS_forWeights(bool Even, stattools::TUpdateWeights<1> &UpdateWeights);
	void _updateAllZOLS_NotParallel();

	// update model switch v_kl
	void _updateMuWVZ(size_t l, coretools::LogProbability &OldSum, const Storage *Data);
	double _calcLogHUpdateMuWVZ_l(size_t l, double *DiffNewOld, coretools::LogProbability &NewSum,
	                              coretools::LogProbability OldSum, const Storage *Data, const arma::vec &mu_v_OLS,
	                              const arma::vec &Y_l, double sampledMu);
	double _calcLLUpdateMuWVZ_l(size_t l, double NewMu, double *DiffNewOld, coretools::LogProbability &NewSum,
	                            coretools::LogProbability OldSum, const Storage *Data) const;
	std::tuple<arma::vec, arma::vec, double> _proposeMuWVZ_withOLS(size_t l);
	void _sumUpdate_VMuU_K2_K(size_t i, size_t Linear_K0, double DiffMu, double *DiffNewOld) const;
	double _calculateLogProposalRatio_MuWVZ(size_t l, const arma::vec &v_OLS, const arma::vec &Y_l, double sampledMu);
	arma::vec _calculateMuWV_withOLS(const arma::vec &Y_l, size_t NewZ) const;
	double _sumUpdate_Z0_ZD(size_t l, double NewMu, double *DiffNewOld, const Storage *Data) const;
	double _sumUpdate_ZD_Z0(size_t l, double NewMu, double *DiffNewOld, const Storage *Data) const;
	double _sumUpdate_ZD_ZD(size_t l, double NewMu, double *DiffNewOld, const Storage *Data) const;

	// simulator
	void _simulateUnderPrior(Storage *Data) override;
	std::array<double, 2> _getWFromCurve(double ExpectedLogOdds) const;
	std::pair<std::array<double, 2>, std::array<double, 2>> _getWScaledFromCurve(double ScaleX, double ScaleU) const;
	void _simulatePosteriorOdds(const std::vector<double> &ScaleX, const std::vector<double> &ScaleU);
	void _readSimulationParameters(coretools::Probability &Error, coretools::StrictlyPositive<double> &MeanDepth) const;
	std::vector<double> _standardizeSimulatedX();
	genometools::BiallelicGenotype _simulateGenotype(size_t l, size_t i) const;
	size_t _simulateDepth(coretools::StrictlyPositive<double> MeanDepth) const;
	size_t _simulateNumReads_WithReferenceAllele(size_t Depth, const genometools::BiallelicGenotype &TrueGenoTypeGTL,
	                                             coretools::Probability Error) const;
	std::vector<coretools::Probability>
	_calculateGenotypeLikelihoods(size_t Depth, const genometools::BiallelicGenotype &TrueGenoTypeGTL,
	                              coretools::Probability Error) const;
	void _storeInObservation(size_t l, size_t i, Storage *Data,
	                         const std::vector<coretools::Probability> &GenotypeLikelihoods) const;
	void _simulateGenotypeLikelihoods(Storage *Data, coretools::StrictlyPositive<double> MeanDepth,
	                                  coretools::Probability Error, std::vector<std::vector<size_t>> &Depth);
	void _writeGeneticData(Storage *Data, std::vector<std::vector<size_t>> &Depth) const;
	void _writeEnv(stattools::TObservationTyped<TypeUX, 2> *EnvDataPtr) const;

	// debugging
	void _printLLSurfaceVZ_K1();
	void _printLLSurfaceVZ_K2();

public:
	TBangolinPrior(stattools::TParameterTyped<TypeF, 1> *F, stattools::TObservationTyped<TypeUX, 2> *X,
	               stattools::TParameterTyped<TypeUX, 2, true> *U, stattools::TParameterTyped<TypeVW, 2> *V,
	               stattools::TParameterTyped<TypeVW, 1> *W, stattools::TParameterTyped<TypeZ, 1> *Z, size_t K,
	               std::string Prefix);
	~TBangolinPrior() override = default;

	[[nodiscard]] std::string name() const override;

	// initialization
	void initializeInferred() override;
	void setAdditionalCommandLineParameters();

	// estimate initial values
	void estimateInitialPriorParameters() override;

	// update weights
	void burninHasFinished() override;

	// full log densities
	double getSumLogPriorDensity(const Storage &Data) const override;

	// update all hyperprior parameters
	void updateParams() override;

	// EM for z
	void initializeEMParameters() override;
	size_t getMLEmissionState(size_t Index, size_t) const override;
	void calculateEmissionProbabilities(size_t Index, stattools::TDataVector<double, size_t> &Emission) const override;
	void handleStatePosteriorEstimation(size_t Index, const stattools::TDataVector<double, size_t> &Probs) override;

	stattools::TUpdateBase *getPtrToUpdaterMuVZ() { return &_updaterOLS; }
};

#endif // BANGOLIN_TBANGOLINPRIOR_H
