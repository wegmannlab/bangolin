//
// Created by madleina on 22.03.21.
//

#ifndef BANGOLIN_TBANGOLINCORE_H
#define BANGOLIN_TBANGOLINCORE_H

#include "BangolinTypes.h"
#include "TBangolinPrior.h"
#include "TData.h"
#include "genometools/GenomePositions/TDistances.h"
#include "stattools/MCMC/TMCMC.h"
#include "coretools/Main/TParameters.h"
#include "stattools/Priors/TPriorBeta.h"
#include "stattools/Priors/TPriorCategorical.h"
#include "stattools/Priors/TPriorDirichlet.h"
#include "stattools/Priors/TPriorExponential.h"
#include "stattools/Priors/TPriorHMMCategorical.h"
#include "stattools/Priors/TPriorNormal.h"
#include "stattools/Priors/TPriorTwoNormalsMixedModel.h"
#include "stattools/Priors/TPriorUniform.h"
#include "coretools/Main/TTask.h"

//------------------------------------------
// TBangolinModelUVX
//------------------------------------------

class TBangolinModelUVX {
private:
	// parameters and observations: u, v, x
	stattools::TParameterTyped<TypeUX, 2, true> _u;
	stattools::TParameterTyped<TypeVW, 2> _v;
	stattools::TObservationTyped<TypeUX, 2> _x;

	template<typename TypeDef> auto _createDefForSimulation(const std::string &Name, const std::string &Filename) {
		using namespace coretools::instances;
		TypeDef def(Filename);
		double variance = parameters().getParameterWithDefault("variance" + Name, 1.0);
		if (variance == 0.0) {
			if constexpr (std::is_same_v<TypeDef, stattools::TParameterDefinition>) { def.setInitVal("0.0"); }
			def.setPriorParameters("0,10e-10"); // variance = 0 is invalid
		} else {
			def.setPriorParameters("0," + coretools::str::toString(variance));
		}
		logfile().list("Will simulate ", Name, " under a normal distribution with mean = 0 and variance = ", variance,
		               ".");
		return def;
	}

	void _addToDAG(stattools::TDAGBuilder &DAGBuilder);

public:
	TBangolinModelUVX(const std::string &Filename, stattools::TDAGBuilder &DAGBuilder,
	                  coretools::TMultiDimensionalStorage<TypeUX, 2> &StorageX);
	TBangolinModelUVX(const std::string &Filename, stattools::TDAGBuilder &DAGBuilder,
					  coretools::TMultiDimensionalStorage<TypeUX, 2> &StorageX, bool Simulate);

	// getters
	stattools::TParameterTyped<TypeUX, 2, true> *u() { return &_u; }
	stattools::TObservationTyped<TypeUX, 2> *x() { return &_x; }
	stattools::TParameterTyped<TypeVW, 2> *v() { return &_v; }
};

//------------------------------------------
// TBangolinModelWZ
//------------------------------------------

class TBangolinModelWZ {
private:
	// parameters: pi, gamma, z
	std::shared_ptr<stattools::prior::TBetaFixed<stattools::TParameterBase, TypePi, 1>> _priorOnPi;
	std::unique_ptr<stattools::TParameterTyped<TypePi, 1>> _pi;
	std::unique_ptr<stattools::TParameterTyped<TypeGamma, 1>> _gamma;
	std::unique_ptr<stattools::TParameterTyped<TypeRhos, 1>> _rhos;
	std::unique_ptr<stattools::TParameterTyped<TypeZ, 1>> _z;

	std::unique_ptr<stattools::TParameterTyped<TypeMuW, 1>> _muW;
	std::unique_ptr<stattools::TParameterTyped<TypeVarsW, 1>> _varW0;
	std::unique_ptr<stattools::TParameterTyped<TypeVarsW, 1>> _varW1;
	std::unique_ptr<stattools::TParameterTyped<TypeVW, 1>> _w;

	void _addToDAG(const std::string &Filename, bool Linkage, bool UniformPriorW, size_t D,
	               stattools::TDAGBuilder &DAGBuilder);

	void _initZ(const std::string &Filename, const std::string &PriorParamsPi, const std::string &PriorParamsRhos,
	            size_t D, genometools::TDistancesBinnedBase *Distances);
	void _initZ(const std::string &Filename, const std::string &PriorParamsRhos, size_t D);
	void _initW(const std::string &Filename, bool UniformPriorW);

public:
	TBangolinModelWZ(const std::string &Filename, stattools::TDAGBuilder &DAGBuilder, bool UniformPriorW,
	                 const std::string &PriorParamsPi, const std::string &PriorParamsRhos, size_t D,
	                 genometools::TDistancesBinnedBase *Distances);
	TBangolinModelWZ(const std::string &Filename, stattools::TDAGBuilder &DAGBuilder, bool UniformPriorW,
	                 const std::string &PriorParamsRhos, size_t D);

	// getters
	stattools::TParameterTyped<TypeZ, 1> *z() { return _z.get(); }
	stattools::TParameterTyped<TypeVW, 1> *w() { return _w.get(); }
};

//------------------------------------------
// TBangolinModelF
//------------------------------------------

class TBangolinModelF {
private:
	// parameters: log_alpha_f, log_beta_f, f
	stattools::TParameterTyped<TypeLogAlphaF, 1> _logAlphaF;
	stattools::TParameterTyped<TypeLogBetaF, 1> _logBetaF;
	stattools::TParameterTyped<TypeF, 1> _f;

public:
	TBangolinModelF(const std::string &Filename, stattools::TDAGBuilder &DAGBuilder, bool Simulate);

	// getters
	stattools::TParameterTyped<TypeF, 1> *f() { return &_f; }
};

//----------------------------------
// TBangolinCore
//----------------------------------

class TBangolinCore {
protected:
	size_t _K = 0;
	std::string _filename;

	virtual void _parseCommandLineArguments();
	virtual void _run(stattools::TDAGBuilder &DAGBuilder, coretools::TMultiDimensionalStorage<TypeUX, 2> &X,
	                  coretools::TMultiDimensionalStorage<TypeGTL, 2> &GTL, TBangolinModelF &ModelF,
	                  TBangolinModelWZ &ModelZ)                                           = 0;
	virtual size_t _readData(coretools::TMultiDimensionalStorage<TypeUX, 2> &X,
	                         coretools::TMultiDimensionalStorage<TypeGTL, 2> &GTL,
	                         genometools::TDistancesBinned<numDistGroupsType> &Distances) = 0;

public:
	void runBangolin(bool Simulate);
};

//----------------------------------
// TBangolin_Inference
//----------------------------------

class TBangolin_Inference : public TBangolinCore {
protected:
	void _parseCommandLineArguments() override;
	void _run(stattools::TDAGBuilder &DAGBuilder, coretools::TMultiDimensionalStorage<TypeUX, 2> &X,
	          coretools::TMultiDimensionalStorage<TypeGTL, 2> &GTL, TBangolinModelF &ModelF,
	          TBangolinModelWZ &ModelZ) override;
	size_t _readData(coretools::TMultiDimensionalStorage<TypeUX, 2> &X,
	                 coretools::TMultiDimensionalStorage<TypeGTL, 2> &GTL,
	                 genometools::TDistancesBinned<numDistGroupsType> &Distances) override;
};

//----------------------------------
// TBangolin_Simulator
//----------------------------------

class TBangolin_Simulator : public TBangolinCore {
protected:
	size_t _n = 0;
	size_t _L = 0;
	size_t _D = 0;

	void _parseCommandLineArguments() override;
	void _simulateNames(coretools::TMultiDimensionalStorage<TypeUX, 2> &X,
	                    coretools::TMultiDimensionalStorage<TypeGTL, 2> &GTL,
	                    genometools::TDistancesBinned<numDistGroupsType> &Distances);

	void _simulateSampleNames(coretools::TMultiDimensionalStorage<TypeUX, 2> &X,
	                          coretools::TMultiDimensionalStorage<TypeGTL, 2> &GTL) const;
	void _simulateEnvNames(coretools::TMultiDimensionalStorage<TypeUX, 2> &X) const;
	void _simulateDistances(genometools::TDistancesBinned<numDistGroupsType> &Distances);

	void _run(stattools::TDAGBuilder &DAGBuilder, coretools::TMultiDimensionalStorage<TypeUX, 2> &X,
	          coretools::TMultiDimensionalStorage<TypeGTL, 2> &GTL, TBangolinModelF &ModelF,
	          TBangolinModelWZ &ModelZ) override;
	size_t _readData(coretools::TMultiDimensionalStorage<TypeUX, 2> &X,
	                 coretools::TMultiDimensionalStorage<TypeGTL, 2> &GTL,
	                 genometools::TDistancesBinned<numDistGroupsType> &Distances) override;
};

//----------------------------------
// TTask_infer
//----------------------------------

class TTask_infer : public coretools::TTask {
public:
	TTask_infer() {
		_citations.insert("Caduff et al. (2020) SOMEWHERE");
		_explanation = "Inferring genotype-environment associations";
	};

	void run() override {
		TBangolin_Inference bangolin;
		bangolin.runBangolin(false);
	};
};

//----------------------------------
// TTask_simulate
//----------------------------------

class TTask_simulate : public coretools::TTask {
public:
	TTask_simulate() {
		_citations.insert("Caduff et al. (2020) SOMEWHERE");
		_explanation = "Simulating under Bangolin-model";
	};

	void run() override {
		TBangolin_Simulator bangolin;
		bangolin.runBangolin(true);
	};
};

#endif // BANGOLIN_TBANGOLINCORE_H
