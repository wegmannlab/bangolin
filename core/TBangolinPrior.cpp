
#include "TBangolinPrior.h"
#include <algorithm>
#include <execution>
#include <utility>

using namespace coretools::instances;

//-------------------------------------------
// TBangolinPrior
//-------------------------------------------

TBangolinPrior::TBangolinPrior(stattools::TParameterTyped<TypeF, 1> *F, stattools::TObservationTyped<TypeUX, 2> *X,
                               stattools::TParameterTyped<TypeUX, 2, true> *U, stattools::TParameterTyped<TypeVW, 2> *V,
                               stattools::TParameterTyped<TypeVW, 1> *W, stattools::TParameterTyped<TypeZ, 1> *Z,
                               size_t K, std::string Prefix)
    : _f(F), _x(X), _u(U), _v(V), _w(W), _z(Z), _K(K), _prefix(std::move(Prefix)) {
	this->addPriorParameter({_f, _x, _u, _v, _w, _z});
	setAdditionalCommandLineParameters();

#ifdef _OPENMP
	_maxNumThreads = coretools::instances::parameters().getParameterWithDefault("maxNumThreads", omp_get_max_threads());
	coretools::instances::logfile().list("Running multi-threaded bangolin with a maximum of ", _maxNumThreads,
	                                     " threads (argument 'maxNumThreads')");
#endif
}

std::string TBangolinPrior::name() const { return "bangolin"; }

void TBangolinPrior::_extractDimensionsFromGTLs() {
	auto data = this->_storageBelow[0];
	_L        = data->dimensions()[0];
	if (_n != data->dimensions()[1]) { // check if n from x and data match
		DEVERROR("The number of individuals of GTL and x do not match (", _n, " vs ", data->dimensions()[1], ")!");
	}
}

void TBangolinPrior::_extractDimensionsFromData() {
	// x: D times n matrix
	_D = _x->dimensions()[0];
	_n = _x->dimensions()[1];

	// parameter: only defined on one observation
	if (this->_storageBelow.size() != 1) {
		DEVERROR("Bangolin prior must be defined on exactly one observation of genetic data (not ",
		         this->_storageBelow.size(), ")!");
	}
	_extractDimensionsFromGTLs();
}

void TBangolinPrior::initializeInferred() {
	_extractDimensionsFromData();

	// get names
	auto data              = this->_storageBelow[0];
	const auto &lociNames  = data->getDimensionName(0);
	const auto &indivNames = data->getDimensionName(1);
	// const auto &envVarNames = _x->getDimensionName(0);
	const auto latFacNames = std::make_shared<coretools::TNamesIndices>();

	// initialize storage of prior parameters accordingly
	// f: linear of length L
	_f->initStorage({_L}, {lociNames});

	// u: K times n matrix
	_u->initStorage({_K, _n}, {latFacNames, indivNames});
	_u->setSizeDataForLogH(_L);

	// v: L times K-1 matrix
	if (_K == 0) {
		_v->initStorage({_L, 0}, {lociNames, latFacNames});
	} else {
		_v->initStorage({_L, _K - 1}, {lociNames, latFacNames});
	}

	// w: linear of length L
	_w->initStorage({_L}, {lociNames});

	// z: linear of length L
	_z->initStorage({_L}, {lociNames});

	// initialize mus
	_mus.resize(_L, 0.);

	// initialize updater
	_updaterOLS.initialize(_L, true, "OLS", _prefix);

	// initialize temporary matrices
	_curXUWithIntercept.resize(_D);
	_curtXUWithIntercept.resize(_D);

	// update weights: provided in file?
	if (parameters().parameterExists("pNotNeutral")) {
		_calculateUpdateWeights = false;
		std::string filename    = parameters().getParameterFilename("pNotNeutral");
		_setInitialUpdateWeights(stattools::UpdatingScheme::powerStatePosterior);
		_readInitialUpdateWeights(filename);
	} else {
		// initialize uniform update weights for burnin
		_setInitialUpdateWeights(stattools::UpdatingScheme::regular);
	}

	// also update z in a regular way (in addition to OLS)?
	_updateZRegularly = parameters().parameterExists("updateZRegularly");
}

void TBangolinPrior::_readInitialUpdateWeights(const std::string &Filename) {
	// read P_not_neutral from file
	coretools::TInputFile file(Filename, coretools::TFile_Filetype::fixed);
	std::vector<double> pNotNeutral;
	while (file.read(pNotNeutral)) {}
	if (pNotNeutral.size() != _L) {
		UERROR("Size of pNotNeutral (", pNotNeutral.size(), ") does not match the number of loci (", _L, ")!");
	}

	_setUpdateWeightsMuVWZ(pNotNeutral);
}

void TBangolinPrior::_setInitialUpdateWeights(stattools::UpdatingScheme::MCMCUpdatingScheme UpdatingScheme) {
	// if run on multiple threads: can not update two neighbouring loci for z-update because of HMM
	// -> first draw even indices, then draw uneven indices -> no racetime possible
	// -> store separate update weights
	// if run on one thread only: no need to do this, only overhead
	if (_maxNumThreads > 1) {
		const size_t numOdd = _L / 2;
		_updateWeightsMuWZ_Even.initializeUpdateWeights({_L - numOdd}, {UpdatingScheme}, "OLS");
		_updateWeightsMuWZ_Odd.initializeUpdateWeights({numOdd}, {UpdatingScheme}, "OLS");
	} else {
		_updateWeightsMuWZ_Even.initializeUpdateWeights({_L}, {UpdatingScheme}, "OLS");
	}
}

void TBangolinPrior::burninHasFinished() {
	// initialize update weights
	if (parameters().parameterExists("uniformUpdateWeights")) {
		// burnin already uses uniform update weights -> no need to re-initialize
		logfile().list("Will update loci for update of mu/w/v/z regularly (no weighted updates).");
	} else {
		if (!parameters().parameterExists("prop_updateWeights_OLS")) { // don't override user-defined values
			double propZ = parameters().getParameterWithDefault("prop_updateWeights_OLS", 0.5);
			logfile().list("Will update OLS mu/w/v/z ", propZ * 100., "% as often as other updates.");
		}

		if (_calculateUpdateWeights) {
			_setInitialUpdateWeights(stattools::UpdatingScheme::powerStatePosterior);
			auto pNotNeutral = _calculatePNotNeutral();
			_setUpdateWeightsMuVWZ(pNotNeutral);
		}
	}
}

void TBangolinPrior::setAdditionalCommandLineParameters() {
	// after how many iterations do we re-calculate logit-fli? (flating point precision decreases over time)
	_numIterUntilResettingLogitFli = parameters().getParameterWithDefault("numIterUntilResettingLogitFli", 10000);
}

coretools::Probability TBangolinPrior::_calculateLikelihood(const Storage *Data, size_t l, size_t i,
                                                            coretools::Probability f_li) const {
	// calculates likelihood:
	// P(D_li | f_li) = sum_g P(D_li | g) P(g | f_li)
	return (*Data)[{l, i}].HWESum<coretools::Probability>(f_li);
}

size_t TBangolinPrior::_getIndexV(size_t l, size_t k) const {
	return _v->getIndex({l, k - 1}); // k-1 because first k is w
}

size_t TBangolinPrior::_getIndexXForZ(size_t Z, size_t i) const {
	if (Z == 0) { DEVERROR("Can not be zero!"); } // TODO: remove after debugging
	const auto d = Z - 1;                         // -1 because z=0 means neutral (take u); z=1 means first env var etc.
	return _x->getIndex({d, i});
}

size_t TBangolinPrior::_getIndexX(size_t l, size_t i) const { return _getIndexXForZ(_z->value(l), i); }

double TBangolinPrior::_sumVlUi_2_K(size_t i, size_t l) const {
	// sum v_l * u_i for all k = 2, 3, ..., K
	double sum = 0.;
	for (size_t k = 1; k < _K; k++) { sum += _v->value(_getIndexV(l, k)) * _u->value(_u->getIndex({k, i})); }
	return sum;
}

double TBangolinPrior::_sumVlUi_ZeroModel(size_t i, size_t l) const {
	double sum = _w->value(l) * _u->value(_u->getIndex({0, i}));
	return sum + _sumVlUi_2_K(i, l);
}

double TBangolinPrior::_sumVlUi_NonZeroModel(size_t i, size_t l, size_t Z) const {
	double sum = _w->value(l) * _x->value(_getIndexXForZ(Z, i));
	return sum + _sumVlUi_2_K(i, l);
}

coretools::LogProbability TBangolinPrior::_calculateSumOverIndividuals(size_t l, const Storage *Data) const {
	coretools::TSumLogProbability sum;
	for (size_t i = 0; i < _n; i++) {
		sum.add(_calculateLikelihood(Data, l, i, TLogisticLookup::approxLogistic(_logit_f_li(l, i))));
	}
	return sum.getSum();
}

double TBangolinPrior::_calculateLogit_f_li(size_t l, size_t i) const {
	return _calculateLogit_f_li(l, i, _z->value(l));
}

double TBangolinPrior::_calculateLogit_f_li(size_t l, size_t i, size_t Z) const {
	if (Z == 0) { return _mus[l] + _sumVlUi_ZeroModel(i, l); }
	return _mus[l] + _sumVlUi_NonZeroModel(i, l, Z);
}

double TBangolinPrior::_calculateLogit_f_li_minus_mu_l(size_t l, size_t i) const {
	return _calculateLogit_f_li_minus_mu_l(l, i, _z->value(l));
}

double TBangolinPrior::_calculateLogit_f_li_minus_mu_l(size_t l, size_t i, size_t Z) const {
	if (Z == 0) { return _sumVlUi_ZeroModel(i, l); }
	return _sumVlUi_NonZeroModel(i, l, Z);
}

void TBangolinPrior::_setLogitFli() {
	for (size_t l = 0; l < _L; l++) {
		for (size_t i = 0; i < _n; i++) { _logit_f_li(l, i) = _calculateLogit_f_li(l, i); }
	}
}

void TBangolinPrior::_initializeLogitFli() {
	_logit_f_li.resize(std::array<size_t, 2>({_L, _n}));
	_setLogitFli();
}

void TBangolinPrior::_updateLoci() {
	// go over all loci and update f and v
	auto data = this->_storageBelow[0];
#pragma omp parallel for num_threads(_maxNumThreads)
	for (size_t l = 0; l < _L; l++) {
		coretools::LogProbability sum = _calculateSumOverIndividuals(l, data);

		// update f
		if (_f->updateSpecificIndex(l)) { _updateF_l(l, sum, data); }

		// update vw
		_updateVW(l, sum, data);
	}
}

void TBangolinPrior::_updateAllZRegularly() {
	if (_updateZRegularly) {
		auto data = this->_storageBelow[0];
		// Note: updating z would have to be parallelized even/uneven indices (HMM) -> just don't do it currently, as it
		// is anyways only a debugging thing!
		for (size_t l = 0; l < _L; l++) {
			// update z (if w,f,v are fixed)
			coretools::LogProbability sum = _calculateSumOverIndividuals(l, data);
			_updateZ(l, sum, data);
		}
	}
}

void TBangolinPrior::_updateAllZOLS_Indices(bool Even, const std::vector<size_t> &Indices) {
	// Can not use OpenMP to parallelize over elements in set:
	// -> elements in set can only be looped over (operator[] does not exist) -> need operator !=
	// -> use GCC for-each parallel policy instead! par_unseq means parallel unsequenced (order does not matter)
	auto data = this->_storageBelow[0];
#pragma omp parallel for num_threads(_maxNumThreads)
	for (auto &index : Indices) {
		size_t l                      = (Even) ? index * 2 : index * 2 + 1;
		coretools::LogProbability sum = _calculateSumOverIndividuals(l, data);
		_updateMuWVZ(l, sum, data);
	}
}

std::pair<bool, std::vector<size_t>> TBangolinPrior::_getIndicesZUpdate(stattools::TUpdateWeights<1> &UpdateWeights) {
	// sample as many indices as there are threads
	std::vector<size_t> indices;
	indices.reserve(_maxNumThreads);

	while (indices.size() < _maxNumThreads) {
		// randomly sample index. If we reached end (or max num updates): return!
		if (!UpdateWeights.update()) { return std::make_pair(false, indices); }
		size_t index = UpdateWeights.getUpdatedLinearIndex();
		if (std::find(indices.begin(), indices.end(), index) == indices.end()) { // make sure each index is unique
			indices.push_back(index);
		}
	}
	return std::make_pair(true, indices);
}

void TBangolinPrior::_updateAllZOLS_forWeights(bool Even, stattools::TUpdateWeights<1> &UpdateWeights) {
	// update
	bool doContinue = true;
	std::vector<size_t> indices;

	while (doContinue) {
		std::tie(doContinue, indices) = _getIndicesZUpdate(UpdateWeights);
		_updateAllZOLS_Indices(Even, indices);
	}
}

void TBangolinPrior::_updateAllZOLS_NotParallel() {
	auto data = this->_storageBelow[0];

	while (_updateWeightsMuWZ_Even.update()) {
		const size_t l                = _updateWeightsMuWZ_Even.getUpdatedLinearIndex();
		coretools::LogProbability sum = _calculateSumOverIndividuals(l, data);
		_updateMuWVZ(l, sum, data);
	}
}

void TBangolinPrior::_updateAllZOLS() {
	if (_maxNumThreads == 1) {
		_updateAllZOLS_NotParallel();
	} else {
		// parallel: make sure that
		// 	1) no neighbouring loci are updated in parallel (HMM -> racetime);
		// 	2) don't update same locus twice in parallel loop (update weights sample with replacement);
		//  3) get maximum parallel capacity
		_updateAllZOLS_forWeights(true, _updateWeightsMuWZ_Even);
		_updateAllZOLS_forWeights(false, _updateWeightsMuWZ_Odd);
	}
}

void TBangolinPrior::updateParams() {
	_addToCounterReSetLogitFli();

	// fill armadillo matrices
	_updateCurArmadilloMatUAndXU(); // shared among all loci -> calculate only once

	_updateLoci();
	_updateAllZRegularly();
	_updateAllZOLS();
	_updateU();
}

void TBangolinPrior::_addToCounterReSetLogitFli() {
	if (_counterIterationsLogitFli == _numIterUntilResettingLogitFli) {
		_setLogitFli();
		_counterIterationsLogitFli = 0;
	} else {
		_counterIterationsLogitFli++;
	}
}

void TBangolinPrior::_printLLSurfaceVZ_K1() {
	// construct grid on v
	if (_K != 1) { DEVERROR("Only implemented for K=1!"); }
	_updateCurArmadilloMatUAndXU();
	size_t numGridPoints = parameters().getParameterWithDefault("numGridPoints", 1000);
	double min           = parameters().getParameterWithDefault("min", -20.);
	double max           = parameters().getParameterWithDefault("max", 8.);
	double step          = (max - min) / ((double)numGridPoints - 1.);
	std::vector<double> grid(numGridPoints);
	for (size_t g = 0; g < numGridPoints; g++) { grid[g] = min + (double)g * step; }

	// calculate LL surface under all values of z
	auto data = _storageBelow[0];
	for (size_t l = 0; l < _L; l++) {
		// assemble header
		std::vector<std::string> header = {"grid", "realW"};
		for (size_t z = 0; z < _D + 1; z++) {
			header.push_back("LL_z" + coretools::str::toString(z));
			header.push_back("w_OLS_z" + coretools::str::toString(z));
		}

		coretools::TOutputFile file(_prefix + "_LL_surface_" + coretools::str::toString(l) + ".txt", header);

		// first do OLS with actual w
		double realW = _w->value(l);
		arma::vec y(_n);
		for (size_t i = 0; i < _n; i++) { y(i) = _data_fli[coretools::getLinearIndex<size_t, 2>({l, i}, {_L, _n})]; }

		// do OLS for all z
		std::vector<arma::vec> mu_w_z_OLS(_D + 1);
		for (size_t z = 0; z < _D + 1; z++) { mu_w_z_OLS[z] = _calculateMuWV_withOLS(y, z); }

		// for LL: loop over all grid points
		for (size_t g = 0; g < numGridPoints; g++) {
			file << grid[g] << realW;

			_w->set(l, grid[g]);
			for (size_t z = 0; z < _D + 1; z++) {
				coretools::TSumLogProbability sum;
				for (size_t i = 0; i < _n; i++) {
					_mus[l]          = mu_w_z_OLS[z][0];
					double logit_fli = _calculateLogit_f_li(l, i, z);
					sum.add(_calculateLikelihood(data, l, i, TLogisticLookup::approxLogistic(logit_fli)));
				}
				file << sum.getSum() << mu_w_z_OLS[z][1];
			}
			file.endln();
		}
		file.close();
	}
}

void TBangolinPrior::_printLLSurfaceVZ_K2() {
	using namespace coretools::str;

	// construct grid on v
	if (_K != 2) { DEVERROR("Only implemented for K=2!"); }
	_updateCurArmadilloMatUAndXU();
	size_t numGridPoints = parameters().getParameterWithDefault("numGridPoints", 50);
	double min           = parameters().getParameterWithDefault("min", -30.);
	double max           = parameters().getParameterWithDefault("max", 30.);
	double step          = (max - min) / ((double)numGridPoints - 1.);
	std::vector<double> grid(numGridPoints);
	for (size_t g = 0; g < numGridPoints; g++) { grid[g] = min + (double)g * step; }

	// calculate LL surface under z=0 and z=1 model
	auto data = _storageBelow[0];
	for (size_t l = 0; l < _L; l++) {
		// assemble header
		std::vector<std::string> headerOLS = {"realW", "realV"};
		for (size_t z = 0; z < _D + 1; z++) {
			headerOLS.push_back("w_OLS_z" + toString(z));
			headerOLS.push_back("v_OLS_z" + toString(z));
		}
		coretools::TOutputFile OLSFile(_prefix + "_OLS_" + toString(l) + ".txt", headerOLS);

		// first do OLS with actual w and v
		std::vector<double> realWVs = {_w->value(l), _v->value(_getIndexV(l, 1))};
		arma::vec y(_n);
		for (size_t i = 0; i < _n; i++) { y(i) = _data_fli[coretools::getLinearIndex<size_t, 2>({l, i}, {_L, _n})]; }

		std::vector<arma::vec> mu_w_z_OLS(_D + 1);
		for (size_t z = 0; z < _D + 1; z++) { mu_w_z_OLS[z] = _calculateMuWV_withOLS(y, z); }

		OLSFile << realWVs[0] << realWVs[1];
		for (size_t z = 0; z < _D + 1; z++) { OLSFile << mu_w_z_OLS[z][1] << mu_w_z_OLS[z][2]; } // w and v
		OLSFile.endln();
		OLSFile.close();

		// for LL: loop over all grid points
		std::vector<std::string> header(numGridPoints);
		for (size_t g = 0; g < numGridPoints; g++) { header[g] = toString(grid[g]); }
		std::vector<coretools::TOutputFile> files(_D + 1);
		for (size_t z = 0; z < _D + 1; z++) {
			files[z].open(_prefix + "_LL_surface_" + toString(l) + "_z" + toString(z) + ".txt", header);
		}

		for (size_t g_k0 = 0; g_k0 < numGridPoints; g_k0++) {
			_w->set(l, grid[g_k0]); // set w
			for (size_t g_k1 = 0; g_k1 < numGridPoints; g_k1++) {
				_v->set(_getIndexV(l, 1), grid[g_k1]); // set v

				for (size_t z = 0; z < _D + 1; z++) {
					coretools::TSumLogProbability sum;
					for (size_t i = 0; i < _n; i++) {
						_mus[l]          = mu_w_z_OLS[z][0];
						double logit_fli = _calculateLogit_f_li(l, i, z);
						sum.add(_calculateLikelihood(data, l, i, TLogisticLookup::approxLogistic(logit_fli)));
					}
					files[z] << sum.getSum();
				}
			}
			for (size_t z = 0; z < _D + 1; z++) { files[z].endln(); }
		}
		for (size_t z = 0; z < _D + 1; z++) { files[z].close(); }
	}
}

//---------------------------------------------
// Update F
//---------------------------------------------

double TBangolinPrior::_calcLLUpdateF_l(size_t l, double &NewMu, double &DiffNewOld, coretools::LogProbability &NewSum,
                                        coretools::LogProbability OldSum, const Storage *Data) const {
	NewMu      = coretools::logit((double)_f->value(l));
	DiffNewOld = NewMu - _mus[l];

	coretools::TSumLogProbability sum;
	for (size_t i = 0; i < _n; i++) {
		sum.add(_calculateLikelihood(Data, l, i, TLogisticLookup::approxLogistic(_logit_f_li(l, i) + DiffNewOld)));
	}
	NewSum = sum.getSum();
	return (double)NewSum - (double)OldSum;
}

double TBangolinPrior::_calcLogHUpdateF_l(size_t l, double &NewMu, double &DiffNewOld,
                                          coretools::LogProbability &NewSum, coretools::LogProbability OldSum,
                                          const Storage *Data) const {
	return _calcLLUpdateF_l(l, NewMu, DiffNewOld, NewSum, OldSum, Data) + _f->getLogPriorRatio(l);
}

void TBangolinPrior::_updateF_l(size_t l, coretools::LogProbability &OldSum, const Storage *Data) {
	// define variables (will be filled inside _calcLLUpdateF_l)
	double diffNewOld                = 0.;
	coretools::LogProbability newSum = 0.;
	double newMu                     = 0.;
	// compute log(Hastings ratio)
	double logH                      = _calcLogHUpdateF_l(l, newMu, diffNewOld, newSum, OldSum, Data);

	if (_f->acceptOrReject(logH, l)) {
		// accepted -> update logit_f_li, sum and mu
		for (size_t i = 0; i < _n; i++) { _logit_f_li(l, i) += diffNewOld; }
		OldSum  = newSum;
		_mus[l] = newMu;
	}
}

//---------------------------------------------
// Update U
//---------------------------------------------

void TBangolinPrior::_updateU() {
	if (_u->isUpdated()) {
		// create pairwise indices to sample
		stattools::TIndexSampler sampler(_n);
		sampler.sampleIndices();

		for (size_t k = 0; k < _K; k++) {
#pragma omp parallel for num_threads(_maxNumThreads)
			for (size_t p = 0; p < sampler.length(); p++) {
				// pick two indices i to update
				auto [i, j] = sampler.getIndexPair(p);
				_updateU_k(i, j, k);
			}
		}
	}
}

bool TBangolinPrior::_locusIsRelevantForUpdateU(size_t k, size_t l) const {
	return k > 0 || (k == 0 && _z->value(l) == 0);
}

double TBangolinPrior::_calcDiffNewOldUpdateU(size_t Linear, double V_lk) const {
	return (_u->value(Linear) - _u->oldValue(Linear)) * V_lk;
}

double TBangolinPrior::_calculateLikRatioU(size_t i, size_t l, size_t Linear, double V_lk, const Storage *Data) const {
	const double diffNewOld = _calcDiffNewOldUpdateU(Linear, V_lk);
	const double lik_new =
	    _calculateLikelihood(Data, l, i, TLogisticLookup::approxLogistic(_logit_f_li(l, i) + diffNewOld));
	const double lik_old = _calculateLikelihood(Data, l, i, TLogisticLookup::approxLogistic(_logit_f_li(l, i)));
	return lik_new / lik_old;
}

double TBangolinPrior::_calcHastingsRatioUpdateU_k(size_t Locus, const coretools::TRange &IxParameters) const {
	auto data                = this->_storageBelow[0];
	const size_t linear_i    = IxParameters.begin;
	const size_t linear_j    = IxParameters.begin + IxParameters.increment;
	std::array<size_t, 2> ki = _u->getSubscripts(linear_i);
	std::array<size_t, 2> kj = _u->getSubscripts(linear_j);

	if (ki[0] != kj[0]) { DEVERROR("k not the same: ", ki[0], " vs ", kj[0]); } // TODO: remove after debugging
	const size_t i = ki[1];
	const size_t j = kj[1];
	const size_t k = ki[0];

	if (_locusIsRelevantForUpdateU(k, Locus)) {
		const double v_lk = (k > 0) ? _v->value(_getIndexV(Locus, k)) : _w->value(Locus);
		return _calculateLikRatioU(i, Locus, linear_i, v_lk, data) *
		       _calculateLikRatioU(j, Locus, linear_j, v_lk, data);
	}
	return 1.0;
}

void TBangolinPrior::_updateU_k(size_t i, size_t j, size_t k) {
	// get linear indices of i and j
	const size_t linear_i = _u->getIndex({k, i});
	const size_t linear_j = _u->getIndex({k, j});

	// update pair
	_u->updatePairWiseUnitVector(linear_i, linear_j);

	// get updated indices: only first element is proposed, second one is deterministically adjusted
	const coretools::TRange parametersIxs(linear_i, linear_j);
	const coretools::TRange updatedIxs(linear_i);

	double logH = 0.0;
	for (size_t l = 0; l < _L; ++l) { logH += _calcHastingsRatioUpdateU_k(l, parametersIxs); }

	// TODO: Change back to logH updating scheme!
	// auto f = &TBangolinPrior::_calcHastingsRatioUpdateU_k;
	// if (_u->acceptOrReject<false>(*this, f, parametersIxs, updatedIxs)) {
	if (_u->acceptOrReject(logH, parametersIxs, updatedIxs)) {
		// accepted -> update logit_f_li
		for (size_t l = 0; l < _L; l++) {
			if (_locusIsRelevantForUpdateU(k, l)) {
				double v_lk = (k > 0) ? _v->value(_getIndexV(l, k)) : _w->value(l);

				_logit_f_li(l, i) += _calcDiffNewOldUpdateU(linear_i, v_lk);
				_logit_f_li(l, j) += _calcDiffNewOldUpdateU(linear_j, v_lk);
			}
		}
	}
}

//---------------------------------------------
// Update V (regular)
//---------------------------------------------

void TBangolinPrior::_updateVW(size_t l, coretools::LogProbability &OldSum, const Storage *Data) {
	if (_K > 0) {
		// update w
		if (_w->updateSpecificIndex(l)) { _updateW_l(l, OldSum, Data); }

		// update v's
		for (size_t k = 1; k < _K; k++) {
			size_t index = _getIndexV(l, k);
			if (_v->updateSpecificIndex(index)) { _updateV_lk(index, l, k, OldSum, Data); }
		}
	}
}

double TBangolinPrior::_calcLLUpdateV_lk(size_t LinearIndex, size_t l, size_t k, double *DiffNewOld,
                                         coretools::LogProbability &NewSum, coretools::LogProbability OldSum,
                                         const Storage *Data) const {
	coretools::TSumLogProbability sum;
	for (size_t i = 0; i < _n; i++) {
		DiffNewOld[i] = (_v->value(LinearIndex) - _v->oldValue(LinearIndex)) * _u->value(_u->getIndex({k, i}));
		sum.add(_calculateLikelihood(Data, l, i, TLogisticLookup::approxLogistic(_logit_f_li(l, i) + DiffNewOld[i])));
	}
	NewSum = sum.getSum();

	// return LL ratio
	return (double)NewSum - (double)OldSum;
}

double TBangolinPrior::_calcLogHUpdateV_lk(size_t LinearIndex, size_t l, size_t k, double *DiffNewOld,
                                           coretools::LogProbability &NewSum, coretools::LogProbability OldSum,
                                           const Storage *Data) const {
	const auto ratio = _calcLLUpdateV_lk(LinearIndex, l, k, DiffNewOld, NewSum, OldSum, Data);
	const auto p     = _v->getLogPriorRatio(LinearIndex);
	return ratio + p;
}

void TBangolinPrior::_updateV_lk(size_t LinearIndex, size_t l, size_t k, coretools::LogProbability &OldSum,
                                 const Storage *Data) {
	// compute log(Hastings ratio)
	double diffNewOld[_n];
	coretools::LogProbability newSum = 0.;
	double logH                      = _calcLogHUpdateV_lk(LinearIndex, l, k, diffNewOld, newSum, OldSum, Data);

	// accept or reject
	if (_v->acceptOrReject(logH, LinearIndex)) {
		// accepted -> update logit_f_li and sum
		for (size_t i = 0; i < _n; i++) { _logit_f_li(l, i) += diffNewOld[i]; }
		OldSum = newSum;
	}
}

//---------------------------------------------
// Update W (regular)
//---------------------------------------------

double TBangolinPrior::_sumUpdateWX(size_t l, double *DiffNewOld, const Storage *Data) const {
	coretools::TSumLogProbability sum;
	for (size_t i = 0; i < _n; i++) {
		DiffNewOld[i] = (_w->value(l) - _w->oldValue(l)) * _x->value(_getIndexX(l, i));
		sum.add(_calculateLikelihood(Data, l, i, TLogisticLookup::approxLogistic(_logit_f_li(l, i) + DiffNewOld[i])));
	}
	return sum.getSum();
}

double TBangolinPrior::_sumUpdateWU(size_t l, double *DiffNewOld, const Storage *Data) const {
	coretools::TSumLogProbability sum;
	for (size_t i = 0; i < _n; i++) {
		DiffNewOld[i] = (_w->value(l) - _w->oldValue(l)) * _u->value(_u->getIndex({0, i}));
		sum.add(_calculateLikelihood(Data, l, i, TLogisticLookup::approxLogistic(_logit_f_li(l, i) + DiffNewOld[i])));
	}
	return sum.getSum();
}

double TBangolinPrior::_calcLLUpdateW_l(size_t l, double *DiffNewOld, coretools::LogProbability &NewSum,
                                        coretools::LogProbability OldSum, const Storage *Data) const {
	NewSum = 0.;
	if (_z->value(l) == 0) { // use u
		NewSum += _sumUpdateWU(l, DiffNewOld, Data);
	} else { // use x
		NewSum += _sumUpdateWX(l, DiffNewOld, Data);
	}

	// return LL ratio
	return (double)NewSum - (double)OldSum;
}

double TBangolinPrior::_calcLogHUpdateW_l(size_t l, double *DiffNewOld, coretools::LogProbability &NewSum,
                                          coretools::LogProbability OldSum, const Storage *Data) const {
	return _calcLLUpdateW_l(l, DiffNewOld, NewSum, OldSum, Data) + _w->getLogPriorRatio(l);
}

void TBangolinPrior::_updateW_l(size_t l, coretools::LogProbability &OldSum, const Storage *Data) {
	// compute log(Hastings ratio)
	double diffNewOld[_n];
	coretools::LogProbability newSum = 0.;
	double logH                      = _calcLogHUpdateW_l(l, diffNewOld, newSum, OldSum, Data);

	// accept or reject
	if (_w->acceptOrReject(logH, l)) {
		// accepted -> update logit_f_li and sum
		for (size_t i = 0; i < _n; i++) { _logit_f_li(l, i) += diffNewOld[i]; }
		OldSum = newSum;
	}
}

//---------------------------------------------
// Update Mu-W-V-Z
//---------------------------------------------

void TBangolinPrior::_updateCurArmadilloMatUAndXU() {
	// same for all loci -> do once before updating mu-v-z!
	for (size_t d = 0; d < _D; d++) {
		_curXUWithIntercept[d]  = _fillArmadilloXUWithIntercept(d);
		_curtXUWithIntercept[d] = _curXUWithIntercept[d].t();
	}
	_curUWithIntercept  = _fillArmadilloUWithIntercept();
	_curtUWithIntercept = _curUWithIntercept.t();
}

std::tuple<arma::vec, arma::vec, double> TBangolinPrior::_proposeMuWVZ_withOLS(size_t l) {
	arma::vec y_l(_n);
	for (size_t i = 0; i < _n; i++) { y_l(i) = _data_fli[coretools::getLinearIndex<size_t, 2>({l, i}, {_L, _n})]; }
	auto mu_w_v_OLS = _calculateMuWV_withOLS(y_l, _z->value(l));

	// now add normal noise to make proposal reversible
	const auto newMu = _updaterOLS.propose(mu_w_v_OLS[0], l);
	double newF      = TLogisticLookup::approxLogistic(newMu);
	if (newF == 0.0) { newF = 0.00001; }
	if (newF == 1.0) { newF = 0.99999; }
	_f->set(l, newF);

	// add noise to w
	double newW = _updaterOLS.propose(mu_w_v_OLS[1], l);
	_w->set(l, newW);

	// add noise to v
	for (size_t k = 1; k < _K; k++) {
		const auto newV = _updaterOLS.propose(mu_w_v_OLS[k + 1], l);
		_v->set(_getIndexV(l, k), newV);
	}

	return std::make_tuple(mu_w_v_OLS, y_l, newMu);
}

void TBangolinPrior::_sumUpdate_VMuU_K2_K(size_t i, size_t l, double DiffMu, double *DiffNewOld) const {
	// k > 0
	for (size_t k = 1; k < _K; k++) {
		size_t index = _getIndexV(l, k);
		DiffNewOld[i] += (_v->value(index) - _v->oldValue(index)) * _u->value(_u->getIndex({k, i}));
	}
	// mu
	DiffNewOld[i] += DiffMu;
}

double TBangolinPrior::_sumUpdate_Z0_ZD(size_t l, double NewMu, double *DiffNewOld, const Storage *Data) const {
	double diffMu = NewMu - _mus[l];
	coretools::TSumLogProbability sum;
	for (size_t i = 0; i < _n; i++) {
		// k = 0
		DiffNewOld[i] = _w->value(l) * _x->value(_getIndexX(l, i)) - _w->oldValue(l) * _u->value(_u->getIndex({0, i}));
		// k > 0 and mu
		_sumUpdate_VMuU_K2_K(i, l, diffMu, DiffNewOld);
		sum.add(_calculateLikelihood(Data, l, i, TLogisticLookup::approxLogistic(_logit_f_li(l, i) + DiffNewOld[i])));
	}
	return sum.getSum();
}

double TBangolinPrior::_sumUpdate_ZD_Z0(size_t l, double NewMu, double *DiffNewOld, const Storage *Data) const {
	double diffMu = NewMu - _mus[l];
	coretools::TSumLogProbability sum;
	for (size_t i = 0; i < _n; i++) {
		// k = 0
		DiffNewOld[i] = _w->value(l) * _u->value(_u->getIndex({0, i})) -
		                _w->oldValue(l) * _x->value(_getIndexXForZ(_z->oldValue(l), i));
		// k > 0 and mu
		_sumUpdate_VMuU_K2_K(i, l, diffMu, DiffNewOld);
		sum.add(_calculateLikelihood(Data, l, i, TLogisticLookup::approxLogistic(_logit_f_li(l, i) + DiffNewOld[i])));
	}
	return sum.getSum();
}

double TBangolinPrior::_sumUpdate_ZD_ZD(size_t l, double NewMu, double *DiffNewOld, const Storage *Data) const {
	double diffMu = NewMu - _mus[l];
	coretools::TSumLogProbability sum;
	for (size_t i = 0; i < _n; i++) {
		// k = 0
		DiffNewOld[i] = _w->value(l) * _x->value(_getIndexX(l, i)) -
		                _w->oldValue(l) * _x->value(_getIndexXForZ(_z->oldValue(l), i));
		// k > 0 and mu
		_sumUpdate_VMuU_K2_K(i, l, diffMu, DiffNewOld);
		sum.add(_calculateLikelihood(Data, l, i, TLogisticLookup::approxLogistic(_logit_f_li(l, i) + DiffNewOld[i])));
	}
	return sum.getSum();
}

double TBangolinPrior::_calcLLUpdateMuWVZ_l(size_t l, double NewMu, double *DiffNewOld,
                                            coretools::LogProbability &NewSum, coretools::LogProbability OldSum,
                                            const Storage *Data) const {
	const size_t oldZ = _z->oldValue(l);
	const size_t newZ = _z->value(l);
	if (oldZ == newZ) { DEVERROR("What a waste..."); }
	if (oldZ == 0 && newZ != 0) {
		// updated u -> x
		NewSum = _sumUpdate_Z0_ZD(l, NewMu, DiffNewOld, Data);
	} else if (oldZ != 0 && newZ == 0) {
		// updated x -> u
		NewSum = _sumUpdate_ZD_Z0(l, NewMu, DiffNewOld, Data);
	} else {
		// updated x -> x
		NewSum = _sumUpdate_ZD_ZD(l, NewMu, DiffNewOld, Data);
	}

	// return LL ratio
	return (double)NewSum - (double)OldSum;
}

double TBangolinPrior::_calcLogHUpdateMuWVZ_l(size_t l, double *DiffNewOld,
                                              coretools::LogProbability &NewSumOverIndividuals,
                                              coretools::LogProbability OldSumOverIndividuals, const Storage *Data,
                                              const arma::vec &mu_w_v_OLS, const arma::vec &Y_l, double sampledMu) {
	// compute log likelihood
	double LL    = _calcLLUpdateMuWVZ_l(l, sampledMu, DiffNewOld, NewSumOverIndividuals, OldSumOverIndividuals, Data);
	// compute prior
	double prior = _w->getLogPriorRatio(l);
	for (size_t k = 1; k < _K; k++) { prior += _v->getLogPriorRatio(_getIndexV(l, k)); }
	prior += _z->getLogPriorRatio(l);
	prior += _f->getLogPriorRatio(l);
	// compute proposal ratio
	double prop = _calculateLogProposalRatio_MuWVZ(l, mu_w_v_OLS, Y_l, sampledMu);

	return LL + prior + prop;
}

void TBangolinPrior::_updateMuWVZ(size_t l, coretools::LogProbability &OldSum, const Storage *Data) {
	if (_w->isUpdated() && _v->isUpdated() && _z->isUpdated() && _f->isUpdated()) {
		// update z
		_z->updateSpecificIndex(l);

		// propose w, v and mu with OLS
		auto [mu_w_v_OLS, y_l, sampledMu] = _proposeMuWVZ_withOLS(l);

		// compute log(Hastings ratio)
		double diffNewOld[_n];
		coretools::LogProbability newSum = 0.;
		double logH = _calcLogHUpdateMuWVZ_l(l, diffNewOld, newSum, OldSum, Data, mu_w_v_OLS, y_l, sampledMu);

		// accept or reject
		if (_z->acceptOrReject(logH, l)) {
			// accepted -> update mu, logit_f_li and sum
			for (size_t i = 0; i < _n; i++) { _logit_f_li(l, i) += diffNewOld[i]; }
			OldSum  = newSum;
			_mus[l] = sampledMu;
		} else {
			// rejected
			_w->reset(l);
			for (size_t k = 1; k < _K; k++) { _v->reset(_getIndexV(l, k)); }
			_f->reset(l);
		}
		// manually add w, v, f to mean/var
		_w->addToMeanVar(l);
		_v->addToMeanVar(l);
		_f->addToMeanVar(l);
	}
}

double TBangolinPrior::_calculateLogProposalRatio_MuWVZ(size_t l, const arma::vec &mu_w_v_OLS, const arma::vec &Y_l,
                                                        double sampledMu) {
	using namespace coretools::probdist;
	arma::vec mu_w_v_OLS_back = _calculateMuWV_withOLS(Y_l, _z->oldValue(l));

	// compute q(x' -> x) / q(x -> x') --> normal density at old location with mean = OLS back
	// 									 / normal density at new location with mean = OLS here

	const double logRatioMu =
	    _updaterOLS.logHastingsRatioPropKernel(l, sampledMu, mu_w_v_OLS[0], _mus[l], mu_w_v_OLS_back[0]);
	const double logRatioW =
	    _updaterOLS.logHastingsRatioPropKernel(l, _w->value(l), mu_w_v_OLS[1], _w->oldValue(l), mu_w_v_OLS_back[1]);

	double logRatioV = 0.0;
	for (size_t k = 1; k < _K; k++) {
		size_t indexV = _getIndexV(l, k);
		logRatioV += _updaterOLS.logHastingsRatioPropKernel(l, _v->value(indexV), mu_w_v_OLS[k + 1],
		                                                    _v->oldValue(indexV), mu_w_v_OLS_back[k + 1]);
	}

	double logRatio = logRatioMu + logRatioW + logRatioV;
	_updaterOLS.addLogProposalRatio(l, logRatio);

	return logRatio;
}

arma::vec TBangolinPrior::_calculateMuWV_withOLS(const arma::vec &Y_l, size_t NewZ) const {
	// fill initial estimate of OLS: zero -> I had sometimes numeric issues when starting with current mu
	arma::vec initial(_K + 1, arma::fill::zeros);

	if (NewZ == 0) {
		// we update z to u-model -> get best mu, w and v for that model!
		return IRLS::doIRLS(Y_l, _curUWithIntercept, _curtUWithIntercept, initial, 0.01, 1);
	} else {
		// we update z to any x-model -> get best mu, w and v for that model!
		return IRLS::doIRLS(Y_l, _curXUWithIntercept[NewZ - 1], _curtXUWithIntercept[NewZ - 1], initial, 0.01, 1);
	}
}

void TBangolinPrior::_updateZ(size_t l, coretools::LogProbability &OldSum, const Storage *Data) {
	if (_z->isUpdated()) {
		_z->updateSpecificIndex(l);

		coretools::TSumLogProbability sumNew;
		for (size_t i = 0; i < _n; i++) {
			sumNew.add(_calculateLikelihood(Data, l, i, TLogisticLookup::approxLogistic(_calculateLogit_f_li(l, i))));
		}
		const double newSum = sumNew.getSum();
		double logH         = newSum - OldSum + _z->getLogPriorRatio(l);

		if (_z->acceptOrReject(logH, l)) {
			for (size_t i = 0; i < _n; i++) { _logit_f_li(l, i) = _calculateLogit_f_li(l, i); }
			OldSum = newSum;
		}
	}
}

double TBangolinPrior::getSumLogPriorDensity(const Storage &Data) const {
	coretools::TSumLogProbability sum;
	for (size_t l = 0; l < _L; l++) {
		for (size_t i = 0; i < _n; i++) {
			sum.add(_calculateLikelihood(&Data, l, i, TLogisticLookup::approxLogistic(_logit_f_li(l, i))));
		}
	}
	return sum.getSum();
}

//-------------------------------------------
// Initialization
//-------------------------------------------

void TBangolinPrior::estimateInitialPriorParameters() {
	if (!_z->hasFixedInitialValue()) { // latent variable not known -> run EM
		_z->runEMEstimation(*this);
	} else {
		initializeEMParameters();
	}

	_initializeTemporaryVariables();
}

void TBangolinPrior::_initializeTemporaryVariables() {
	_mus.resize(_L, 0.);
	for (size_t l = 0; l < _L; l++) { _mus[l] = coretools::logit((double)_f->value(l)); }

	_initializeLogitFli();
}

void TBangolinPrior::_standardizeLogitFli(arma::vec &Logit_f_li) const { Logit_f_li -= arma::mean(Logit_f_li); }

void TBangolinPrior::_fillLogitFli(arma::vec &logit_f_li, size_t l) const {
	// find mean posterior estimate for the allele frequency (= f_li) in logit-scale
	logit_f_li.zeros(_n);
	for (size_t i = 0; i < _n; i++) {
		auto tmp      = _data_fli[coretools::getLinearIndex<size_t, 2>({l, i}, {_L, _n})];
		// cut at some minimal allele frequency (can not be 0 or 1, because logit then yields inf)
		tmp           = std::max(tmp, 0.001);
		tmp           = std::min(tmp, 0.999);
		logit_f_li(i) = coretools::logit(tmp);
	}
}

double TBangolinPrior::_normalizeUk(const coretools::TRange &Range) {
	double sumOfSquares = 0.0;
	for (size_t i = Range.begin; i < Range.end; i += Range.increment) { sumOfSquares += _u->value(i) * _u->value(i); }
	const double vectorNorm = sqrt(sumOfSquares);
	for (size_t i = Range.begin; i < Range.end; i += Range.increment) {
		_u->setVal(i, _u->value(i) / vectorNorm); // use setVal -> do not switch new to old
	}
	return vectorNorm;
}

std::vector<double> TBangolinPrior::_normalizeAllU() {
	std::vector<double> scales(_K, 1.0);
	for (size_t k = 0; k < _K; k++) {
		coretools::TRange range = _u->get1DSlice(1, {k, 0}); // one row
		scales[k]               = _normalizeUk(range);
	}
	return scales;
}

void TBangolinPrior::_estimateInitialU() {
	if (_K == 0) { return; }
	logfile().listFlush("Estimating initial values for u...");
	if (!_u->hasFixedInitialValue()) {
		// initialize u_2, ..., u_K
		_calculateInitialU2_UK();
		// initialize u_1
		_calculateInitialU1();
	} else {
		_normalizeAllU();
	}
	logfile().done();
}

arma::mat TBangolinPrior::_calculateEigenvectors_PCA_U(bool projectX, bool projectU2_UK) const {
	// calculate variance-covariance matrix among individuals
	arma::mat S = _calculateSForInitializationU(projectX, projectU2_UK);

	// perform eigen decomposition
	arma::vec eigval;
	arma::mat eigvec;
	eig_sym(eigval, eigvec, S); // the eigenvalues are in ascending order, eigenvectors are already normalized

	return eigvec;
}

void TBangolinPrior::_calculateInitialU2_UK() {
	// calculate u_2, ..., u_K
	// get u's from eigenvectors and eigenvalues
	// to calculate u_2, ..., u_K: project x out of y_l
	auto eigenvec = _calculateEigenvectors_PCA_U(true, false);

	// first u_k corresponds to last eigenvector
	for (size_t k = 0; k < _K - 1; k++) {
		size_t colInEigenVec = (int)eigenvec.n_cols - (int)k - 1; // go backwards
		// fill parameter u_2, ..., u_K
		for (size_t i = 0; i < _n; i++) {
			double value    = eigenvec(i, colInEigenVec);
			size_t indexInU = _u->getIndex({k + 1, i}); // k=1 -> u_2, k=2 -> u_3 etc.
			_u->set(indexInU, value);
		}
	}
}

void TBangolinPrior::_calculateInitialU1() {
	// calculate u_1
	// get u's from eigenvectors and eigenvalues
	// to calculate u_1: project u_2, ..., u_K out of y_l
	auto eigenvec = _calculateEigenvectors_PCA_U(false, true);

	// first u_k corresponds to last eigenvector
	size_t colInEigenVec = eigenvec.n_cols - 1; // take last eigenvector
	// fill parameter u_1
	for (size_t i = 0; i < _n; i++) {
		double value    = eigenvec(i, colInEigenVec);
		size_t indexInU = _u->getIndex({0, i}); // k=0
		_u->set(indexInU, value);
	}
}

void TBangolinPrior::_projectMatrixOutOfLogitFli(arma::vec &Logit_f_li, const arma::mat &Projection) const {
	Logit_f_li = Projection * Logit_f_li;
}

arma::mat TBangolinPrior::_calculateVectorProjection(const arma::mat &Mat) const {
	arma::mat Mat_t = Mat.t();
	return arma::eye(Mat.n_cols, Mat.n_cols) - Mat_t * arma::inv(Mat * Mat_t) * Mat;
}

void TBangolinPrior::_addToExyAndEx(const arma::vec &logit_f_li, arma::mat &Exy, arma::mat &Ex) const {
	for (size_t i = 0; i < _n; i++) {
		Ex(i) += logit_f_li(i);
		// add to diagonal
		Exy(i, i) += logit_f_li(i) * logit_f_li(i);
		// add to off-diagonal
		for (size_t j = i + 1; j < _n; j++) { Exy(i, j) += logit_f_li(i) * logit_f_li(j); }
	}
}

arma::mat TBangolinPrior::_calculateS(const arma::mat &Exy, const arma::vec &Ex) const {
	arma::mat S(_n, _n, arma::fill::zeros);
	for (size_t i = 0; i < _n; i++) {
		S(i, i) = Exy(i, i) - Ex(i) * Ex(i);
		for (size_t j = i + 1; j < _n; j++) {
			S(i, j) = Exy(i, j) - Ex(i) * Ex(j);
			S(j, i) = S(i, j);
		}
	}
	return S;
}

arma::mat TBangolinPrior::_calculateSForInitializationU(bool projectX, bool projectU2_UK) const {
	arma::mat Exy(_n, _n, arma::fill::zeros); // matrix with expected values
	arma::vec Ex(_n, arma::fill::zeros);      // vector with expected values
	arma::vec logit_f_li(_n);

	arma::mat projection;
	if (projectX) {
		projection = _calculateVectorProjection(_fillArmadilloX());
	} else if (projectU2_UK) {
		projection = _calculateVectorProjection(_fillArmadilloU2_UK());
	}

	for (size_t l = 0; l < _L; l++) {
		_fillLogitFli(logit_f_li, l);
		_standardizeLogitFli(logit_f_li);
		if (projectX || projectU2_UK) { _projectMatrixOutOfLogitFli(logit_f_li, projection); }
		_addToExyAndEx(logit_f_li, Exy, Ex);
	}

	// compute variance-covariance matrix S
	Exy /= (double)_L;
	Ex /= (double)_L;
	return _calculateS(Exy, Ex);
}

arma::mat TBangolinPrior::_fillArmadilloU2_UK() const {
	// fill armadillo matrix U from parameter u2, ..., uK (same as _fillArmadilloU, but without first row)
	arma::mat U2_UK(_K - 1, _n, arma::fill::zeros);
	for (size_t k = 1; k < _K; k++) {
		for (size_t i = 0; i < _n; i++) { U2_UK(k - 1, i) = _u->value(_u->getIndex({k, i})); }
	}
	return U2_UK;
}

arma::mat TBangolinPrior::_fillArmadilloXUWithIntercept(size_t d) const {
	// fill armadillo matrix XU from parameter x and u
	arma::mat XU(_n, _K + 1);
	for (size_t i = 0; i < _n; i++) {
		// first col: 1's (for intercept)
		XU(i, 0) = 1.0;
		// second col: x
		XU(i, 1) = _x->value(_x->getIndex({d, i}));
		// all other cols: u_2, ..., u_K
		for (size_t k = 1; k < _K; k++) { XU(i, k + 1) = _u->value(_u->getIndex({k, i})); }
	}
	return XU;
}

arma::mat TBangolinPrior::_fillArmadilloUWithIntercept() const {
	// fill armadillo matrix U from parameter u
	arma::mat U(_n, _K + 1);
	for (size_t i = 0; i < _n; i++) {
		// first col: 1's (for intercept)
		U(i, 0) = 1.0;
		// all other cols: u_1, ..., u_K
		for (size_t k = 0; k < _K; k++) { U(i, k + 1) = _u->value(_u->getIndex({k, i})); }
	}
	return U;
}

arma::mat TBangolinPrior::_fillArmadilloX() const {
	arma::mat X(_D, _n);
	for (size_t d = 0; d < _D; d++) {
		size_t indexFirstI = _x->getIndex({d, 0});
		for (size_t i = 0; i < _n; i++) { X(d, i) = _x->value(indexFirstI + i); }
	}
	return X;
}

void TBangolinPrior::_setInitialF(size_t l, double Mu) {
	if (!_f->hasFixedInitialValue()) {
		// Note: if f has a fixed initial value, do not standardize Logit_f_li with this, as there is no guarantee
		// that the values are then properly standardized
		coretools::Probability f = coretools::logistic(Mu);
		// make sure f is not too close to boundaries -> otherwise, we are stuck
		f                        = std::max(f, coretools::Probability(0.00001));
		f                        = std::min(f, coretools::Probability(1. - 0.00001));
		_f->set(l, f.get());
		_mus[l] = Mu;
	}
}

void TBangolinPrior::_setInitialVW(const arma::vec &MuV, size_t l) {
	// initialize w
	if (!_w->hasFixedInitialValue() && _K > 0) {
		_w->set(l, MuV(1)); // 1 because first element is mu
	}
	// initialize v
	if (!_v->hasFixedInitialValue() && _K > 0) {
		for (size_t k = 1; k < _K; k++) {
			double value = MuV(k + 1); // +1 because first element is mu
			_v->set(_getIndexV(l, k), value);
		}
	}
}

void TBangolinPrior::_setInitialZ(size_t value, size_t l) {
	if (!_z->hasFixedInitialValue()) { _z->set(l, value); }
}

void TBangolinPrior::_setInitialMuAndVAndZ(size_t l, const std::vector<arma::vec> &Mu_w_v_OLS, const Storage *Data,
                                           std::vector<double> &LLRatios) {
	std::vector<double> LLs(_D + 1);
	// decide which one is better: calculate LL-ratio
	for (size_t z = 0; z < _D + 1; z++) {
		coretools::TSumLogProbability sum;
		arma::mat logit_f_l =
		    (z == 0) ? (Mu_w_v_OLS[z].t() * _curtUWithIntercept) : (Mu_w_v_OLS[z].t() * _curtXUWithIntercept[z - 1]);

		for (size_t i = 0; i < _n; i++) {
			sum.add(_calculateLikelihood(Data, l, i, TLogisticLookup::approxLogistic(logit_f_l(0, i))));
		}
		LLs[z] = sum.getSum();
	}

	// find element with largest LL
	size_t max_z = std::distance(LLs.begin(), std::max_element(LLs.begin(), LLs.end()));
	_setInitialZ(max_z, l);

	// calculate LL Ratio: best z!=0 vs z=0
	size_t max_nonzero_z = std::distance(LLs.begin(), std::max_element(LLs.begin() + 1, LLs.end()));
	if (max_nonzero_z == 0) { DEVERROR("bug!"); } // TODO: remove after debugging
	LLRatios[l] = LLs[max_nonzero_z] - LLs[0];

	// set initial v accordingly
	_setInitialVW(Mu_w_v_OLS[_z->value(l)], l);
	_setInitialF(l, Mu_w_v_OLS[_z->value(l)][0]);
}

void TBangolinPrior::_estimateInitialMuAndVAndZ(const Storage *Data) {
	logfile().listFlush("Estimating initial values for v and z...");

	_updateCurArmadilloMatUAndXU();

	// go over all loci and initialize mu_l and v_l and z_l (on the fly)
	std::vector<double> LLRatios(_L);
	std::vector<arma::vec> mu_w_v_OLS(_D + 1);
	for (size_t l = 0; l < _L; l++) {
		arma::vec y_l(_n);
		for (size_t i = 0; i < _n; i++) { y_l(i) = _data_fli[coretools::getLinearIndex<size_t, 2>({l, i}, {_L, _n})]; }
		for (size_t z = 0; z < _D + 1; z++) { mu_w_v_OLS[z] = _calculateMuWV_withOLS(y_l, z); }

		// initialize w_l and v_l and z_l
		_setInitialMuAndVAndZ(l, mu_w_v_OLS, Data, LLRatios);
	}

	logfile().done();
}

std::vector<double> TBangolinPrior::_calculatePNotNeutral() const {
	// rank loci based on posterior of z not being neutral: 1 - P(z = 0)
	std::vector<double> P_not_neutral(_L);
	for (size_t l = 0; l < _L; l++) { P_not_neutral[l] = 1.0 - _z->statePosteriors(l, 0); }
	// debugging: write to file
	coretools::TOutputFile file(_prefix + "_P_not_neutral.txt");
	file.writeln(P_not_neutral);
	return P_not_neutral;
}

void TBangolinPrior::_setUpdateWeightsMuVWZ(const std::vector<double> &P_not_neutral) {
	if (_maxNumThreads == 1) {
		_updateWeightsMuWZ_Even.setUpdateWeights(0, P_not_neutral, _prefix);
	} else {
		// parallel: make distinction between even and odd indices
		_setUpdateWeightsMuVWZ_Parallel(0, P_not_neutral, _updateWeightsMuWZ_Even, _prefix + "_evenIndices");
		_setUpdateWeightsMuVWZ_Parallel(1, P_not_neutral, _updateWeightsMuWZ_Odd, _prefix + "_oddIndices");
	}
}

void TBangolinPrior::_setUpdateWeightsMuVWZ_Parallel(size_t Start, const std::vector<double> &P_not_neutral,
                                                     stattools::TUpdateWeights<1> &UpdateWeights,
                                                     const std::string &Out) {
	std::vector<double> P_not_neutral_noNeighbours;
	P_not_neutral_noNeighbours.reserve(UpdateWeights.size(0));

	for (size_t l = Start; l < _L; l += 2) { P_not_neutral_noNeighbours.push_back(P_not_neutral[l]); }

	UpdateWeights.setUpdateWeights(0, P_not_neutral_noNeighbours, Out);
}

void TBangolinPrior::_fillDataFli(const Storage *Data) {
	_data_fli.resize(_L * _n);
	for (size_t c = 0; c < _L * _n; ++c) { _data_fli[c] = (*Data)[c].meanPosteriorAlleleFrequency(); }
}

void TBangolinPrior::initializeEMParameters() {
	// get pointer to data
	auto data = this->_storageBelow[0];

	_fillDataFli(data);
	if (parameters().parameterExists("printLLSurface")) {
		if (_K == 1) { _printLLSurfaceVZ_K1(); }
		if (_K == 2) { _printLLSurfaceVZ_K2(); }
	}
	_estimateInitialU();
	_estimateInitialMuAndVAndZ(data);
}

size_t TBangolinPrior::getMLEmissionState(size_t Index, size_t) const { return _z->value(Index); }

void TBangolinPrior::calculateEmissionProbabilities(size_t Index,
                                                    stattools::TDataVector<double, size_t> &Emission) const {
	// HMM runs over loci -> Index corresponds to locus (l)
	auto data = this->_storageBelow[0];

	// go over all individuals and sum log of emission probability
	// we want emission not in log, but we will use the log to sum and then de-log in the end
	for (size_t z = 0; z < _D + 1; z++) {
		coretools::TSumLogProbability sum;
		for (size_t i = 0; i < _n; i++) {
			sum.add(_calculateLikelihood(data, Index, i,
			                             TLogisticLookup::approxLogistic(_calculateLogit_f_li(Index, i, z))));
		}
		Emission[z] = sum.getSum();
	}

	// get rid of log and normalize
	// Note: normalizing is only ok because we don't estimate the emission parameters in the EM!
	// if we would estimate them: max would change -> LL difference among EM iterations would not be meaningful!
	auto maxEmission = Emission.max();
	for (size_t z = 0; z < _D + 1; z++) { Emission[z] = exp(Emission[z] - maxEmission); }
}

void TBangolinPrior::handleStatePosteriorEstimation(
    size_t Index, const stattools::TDataVector<double, size_t> &StatePosteriorProbabilities) {
	// initialize z to posterior mode
	if (!_z->hasFixedInitialValue()) { _z->set(Index, StatePosteriorProbabilities.maxIndex()); }
}

//-------------------------------------------
// Simulator
//-------------------------------------------

void TBangolinPrior::_readSimulationParameters(coretools::Probability &Error,
                                               coretools::StrictlyPositive<double> &MeanDepth) const {
	Error = parameters().getParameterWithDefault("error", 0.05);
	logfile().list("Will use a per allele genotyping error rate of " + coretools::str::toString(Error) + ".");

	MeanDepth = parameters().getParameterWithDefault("meanDepth", 20.);
}

std::vector<double> TBangolinPrior::_standardizeSimulatedX() {
	// standardize x
	std::vector<double> scales(_D, 1.0);
	if (_x->hasFixedInitialValue()) { return scales; }

	// standardize each environmental variable to have unit length
	for (size_t d = 0; d < _D; d++) {
		double sum = 0.0;

		// sum
		for (size_t i = 0; i < _n; i++) {
			const auto index = _x->getIndex({d, i});
			const auto val   = _x->value(index);
			sum += val * val;
		}

		const auto sqrt_sum = sqrt(sum);
		for (size_t i = 0; i < _n; i++) {
			const auto index = _x->getIndex({d, i});
			const auto val   = _x->value(index) / sqrt_sum;
			_x->set(index, val);
		}
		scales[d] = sqrt_sum;
	}
	return scales;
}

std::array<double, 2> TBangolinPrior::_getWFromCurve(double ExpectedLogOdds) const {
	// values obtained by script "simulatePosteriorOdds.R"
	const double cutoff_odds                = 1.16576;
	const std::array<double, 2> coff_quad   = {0.1361660, 0.2267198};
	const std::array<double, 2> coff_linear = {-0.5391040, 0.8454719};

	if (ExpectedLogOdds <= cutoff_odds) {
		// use quadratic
		const double w = (-coff_quad[0] + sqrt(coff_quad[0] * coff_quad[0] + 4 * ExpectedLogOdds * coff_quad[1])) /
		                 (2.0 * coff_quad[1]);
		return {-w, w};
	} else {
		// use linear
		const double w = (ExpectedLogOdds - coff_linear[0]) / coff_linear[1];
		return {-w, w};
	}
}

std::pair<std::array<double, 2>, std::array<double, 2>> TBangolinPrior::_getWScaledFromCurve(double ScaleX,
                                                                                             double ScaleU) const {
	const double expectedLogOdds = parameters().getParameter<double>("expectedLogOdds");
	const auto w                 = _getWFromCurve(expectedLogOdds);

	const std::array<double, 2> w_forX = {w[0] * ScaleX, w[1] * ScaleX};
	const std::array<double, 2> w_forU = {w[0] * ScaleU, w[1] * ScaleU};

	return std::make_pair(w_forX, w_forU);
}

void TBangolinPrior::_simulatePosteriorOdds(const std::vector<double> &ScaleX, const std::vector<double> &ScaleU) {
	if (!parameters().parameterExists("expectedLogOdds")) { return; }
	if (ScaleX.size() != 1 || ScaleU.empty()) { UERROR("Can not simulate posterior odds if D != 1 or K = 0!"); }

	const auto [w_forX, w_forU] = _getWScaledFromCurve(ScaleX[0], ScaleU[0]);

	// set: sample randomly one of the two values
	for (size_t l = 0; l < _L; l++) {
		if (_z->value(l) == 0) {
			_w->set(l, w_forU[randomGenerator().pickOneOfTwo()]);
		} else {
			_w->set(l, w_forX[randomGenerator().pickOneOfTwo()]);
		}
	}
}

genometools::BiallelicGenotype TBangolinPrior::_simulateGenotype(size_t l, size_t i) const {
	// allele frequency
	coretools::Probability f_li = coretools::logistic(_logit_f_li(l, i));

	// draw genotypes from the allele frequencies
	uint8_t random = randomGenerator().getBinomialRand(f_li, 2);
	return genometools::BiallelicGenotype(random);
}

size_t TBangolinPrior::_simulateDepth(coretools::StrictlyPositive<double> MeanDepth) const {
	return randomGenerator().getPoissonRandom(MeanDepth);
}

size_t TBangolinPrior::_simulateNumReads_WithReferenceAllele(size_t Depth,
                                                             const genometools::BiallelicGenotype &TrueGenotype,
                                                             coretools::Probability Error) const {
	if (TrueGenotype == genometools::BiallelicGenotype::homoFirst) {
		return randomGenerator().getBinomialRand(1. - Error, Depth);
	} else if (TrueGenotype == genometools::BiallelicGenotype::het) {
		return randomGenerator().getBinomialRand(0.5, Depth);
	} else {
		return randomGenerator().getBinomialRand(Error, Depth);
	}
}

std::vector<coretools::Probability>
TBangolinPrior::_calculateGenotypeLikelihoods(size_t Depth, const genometools::BiallelicGenotype &TrueGenoTypeGenoLL,
                                              coretools::Probability Error) const {
	std::vector<coretools::Probability> genotypeLikelihoods(3);
	size_t numRef = _simulateNumReads_WithReferenceAllele(Depth, TrueGenoTypeGenoLL, Error);
	int numAlt    = (int)Depth - (int)numRef;

	genotypeLikelihoods[0] = pow(1. - Error, (double)numRef) * pow(Error, numAlt);
	genotypeLikelihoods[1] = pow(0.5, (double)numRef + numAlt);
	genotypeLikelihoods[2] = pow(Error, (double)numRef) * pow(1. - Error, numAlt);

	return genotypeLikelihoods;
}

void TBangolinPrior::_storeInObservation(size_t l, size_t i, Storage *Data,
                                         const std::vector<coretools::Probability> &GenotypeLikelihoods) const {
	size_t linearIndex   = Data->getIndex({l, i});
	(*Data)[linearIndex] = TypeGTL(genometools::PhredIntProbability(GenotypeLikelihoods[0]),
	                               genometools::PhredIntProbability(GenotypeLikelihoods[1]),
	                               genometools::PhredIntProbability(GenotypeLikelihoods[2]));
}

void TBangolinPrior::_simulateGenotypeLikelihoods(Storage *Data, coretools::StrictlyPositive<double> MeanDepth,
                                                  coretools::Probability Error,
                                                  std::vector<std::vector<size_t>> &Depth) {
	Depth.resize(_L, std::vector<size_t>(_n, 0));
	coretools::TOutputFile file(_prefix + "_genotypes.txt");
	for (size_t l = 0; l < _L; ++l) {
		for (size_t i = 0; i < _n; ++i) {
			// simulate genotype and depth
			genometools::BiallelicGenotype genotype = _simulateGenotype(l, i);
			Depth[l][i]                             = _simulateDepth(MeanDepth);
			file << (size_t)genotype;

			// simulate genotype likelihoods
			auto genotypeLikelihoods = _calculateGenotypeLikelihoods(Depth[l][i], genotype, Error);

			// store in observation and write to vcf
			_storeInObservation(l, i, Data, genotypeLikelihoods);
		}
		file.endln();
	}
	file.close();
}

void TBangolinPrior::_simulateUnderPrior(Storage *Data) {
	// read simulation parameters
	coretools::Probability error;
	coretools::StrictlyPositive<double> meanDepth;
	_readSimulationParameters(error, meanDepth);

	// normalize U and X
	const auto scales_X  = _standardizeSimulatedX();
	const auto scales_U1 = _normalizeAllU();

	// simulate W from posterior odds, if needed
	_simulatePosteriorOdds(scales_X, scales_U1);

	// fill logit_f_li
	_initializeTemporaryVariables();

	// get pointer to data
	std::vector<std::vector<size_t>> depth;
	if (Data->hasDefaultValues()) { // only simulate if data has not a fixed value
		_simulateGenotypeLikelihoods(Data, meanDepth, error, depth);
	}

	// write vcf and env files
	_writeGeneticData(Data, depth);
	_writeEnv(_x);
}

void TBangolinPrior::_writeGeneticData(Storage *GeneticData, std::vector<std::vector<size_t>> &Depth) const {
	// first wanted to write vcf in TBangolinCore (analogous to reading data),
	// but I couldn't find an obvious way to pass simulated depth (which might be relevant for filtering)
	// from TBangolinPrior to TBangolinCore
	// -> instead directly write file here!

	// open vcf
	std::string fullName = _prefix + ".vcf.gz";
	logfile().listFlush("Writing vcf to file " + fullName + "...");
	TVCFWriter vcf(GeneticData->getDimensionName(0), GeneticData->getDimensionName(1));

	vcf.openVCF(fullName);
	vcf.writeHeader();

	for (size_t l = 0; l < _L; ++l) {
		vcf.newSite();
		for (size_t i = 0; i < _n; ++i) {
			size_t linearIndex = GeneticData->getIndex({l, i});
			vcf.write((*GeneticData)[linearIndex], Depth[l][i]);
		}
	}
	vcf.closeVcf();
	logfile().done();
}

void TBangolinPrior::_writeEnv(stattools::TObservationTyped<TypeUX, 2> *EnvData) const {
	coretools::TDataWriterBase<TypeUX, 2> writer;
	std::string fullName = _prefix + ".env";
	logfile().listFlush("Writing env to file " + fullName + "...");

	// transpose x
	coretools::TMultiDimensionalStorage<TypeUX, 2> transposed;
	transposed.prepareFillData(_n, {_D});
	for (size_t i = 0; i < _n; i++) {
		for (size_t d = 0; d < _D; d++) {
			auto value = EnvData->value(EnvData->getIndex({d, i}));
			transposed.push_back(value);
		}
	}
	transposed.finalizeFillData();
	// add names
	transposed.setDimensionName(EnvData->getDimensionName(1), 0);
	transposed.setDimensionName(EnvData->getDimensionName(0), 1);

	writer.write(fullName, &transposed, {'\n', '\t'}, {true, true}, coretools::colNames_multiline);
	logfile().done();
}
