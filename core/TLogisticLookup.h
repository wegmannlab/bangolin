/*
 * TLogisticLookup.h
 *
 *  Created on: Oct 16, 2018
 *      Author: caduffm
 */

#ifndef TLOGISTICLOOKUP_H_
#define TLOGISTICLOOKUP_H_

#include "TLogisticLookup.tpp"
#include "coretools/Math/mathFunctions.h"
#include "coretools/Strings/stringFunctions.h"

class TLogisticLookup {
	// approximate the logistic function with a lookup table
private:
	// size corresponds to lookup table constructed with getLogisticLookupTable().
	static constexpr uint16_t _lengthLookup = 2002;
	static constexpr uint16_t _lengthArray  = _lengthLookup / 2;
	static constexpr uint16_t _xsat         = 15;
	static constexpr double _inv_delta_x    = 1. / ((double)_xsat / _lengthArray);
	using array_size                        = std::array<double, _lengthLookup>::size_type;

	static constexpr std::array<double, _lengthLookup> _lookup = getLogisticLookupTable();

public:
	static coretools::Probability approxLogistic(const double x) {
		if (x < 0.0) { // mirror at zero
			const double absx = std::fabs(x);
			if (absx >= _xsat) { return coretools::logistic(x); }
			// avoid using floor (slow) -> absx/delta_x will always be >0, so we can directly cast!
			const auto index = 2 * static_cast<array_size>(absx * _inv_delta_x);
			const double y   = _lookup[index] + _lookup[index + 1] * absx; // linear function
			return 1. - y;
		}
		// x >= 0.0
		if (x >= _xsat) { return coretools::logistic(x); }
		// avoid using floor (slow) -> absx/delta_x will always be >0, so we can directly cast!
		const auto index = 2 * static_cast<array_size>(x * _inv_delta_x);
		const double y   = _lookup[index] + _lookup[index + 1] * x; // linear function
		return y;
	};
};

#endif /* TLOGISTICLOOKUP_H_ */
