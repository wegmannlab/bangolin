//
// Created by madleina on 03.06.22.
//

#ifndef BANGOLIN_TGRAMSCHMIDT_H
#define BANGOLIN_TGRAMSCHMIDT_H

#include "coretools/Storage/TStorage.h"
#include "coretools/algorithms.h"
#include <cassert>
#include <vector>

namespace GramSchmidt {

namespace impl {
template<typename Container> double dotProduct_jk(Container &Vals, size_t j, size_t k, size_t NumCols) {
	// calculate dot product of (Vals_j^T * Vals_k)
	double sum = 0.;
	for (size_t i = 0; i < NumCols; i++) {
		sum += coretools::underlying(Vals(j, i)) * coretools::underlying(Vals(k, i));
	}
	return sum;
}

template<typename ContainerA, typename ContainerB>
double dotProduct_jk(const ContainerA &A, const ContainerB &B, size_t j, size_t k, size_t NumCols) {
	// calculate dot product of (A_j^T * B_k)
	double sum = 0.;
	for (size_t i = 0; i < NumCols; i++) {
		sum += coretools::underlying(A(j, i)) * coretools::underlying(B(k, i));
	}
	return sum;
}

} // namespace impl

template<typename Container>
void gramSchmidt_k(std::vector<double> &Vals_k, const Container &Vals, size_t k, size_t NumRows, size_t NumCols,
                   size_t First = 0) {
	// Assumes that Vals is 2-dimensional with NumRows x NumCols
	// All rows of Vals must be orthogonal to each other, but the k'th row has changed -> make it orthogonal to rest
	// fill result in Vals_k (don't modify Vals)
	// ATTENTION: assumes that Vals_k already contains the current values of the k'th row of Vals!
	assert(Vals_k.size() == NumCols);

	for (size_t j = First; j < NumRows; j++) {
		if (j == k) { continue; }
		double sum = impl::dotProduct_jk(Vals, j, k, NumCols);
		for (size_t i = 0; i < NumCols; i++) { Vals_k[i] -= sum * coretools::underlying(Vals(j, i)); }
	}

	// normalize to unit length
	coretools::normalize(Vals_k, coretools::vectorNorm(Vals_k));
}

template<typename Container, typename ContainerOut>
void gramSchmidt(ContainerOut &Ortho, const Container &Vals, size_t NumRows, size_t NumCols, size_t First = 0) {
	// Assumes that Vals is 2-dimensional with NumRows x NumCols
	// Make all rows of Vals orthogonal to each other
	Ortho = Vals;
	for (size_t k = First; k < NumRows; k++) {
		std::vector<double> ortho_k(NumCols);
		for (size_t i = 0; i < NumCols; i++) { ortho_k[i] = coretools::underlying(Vals(k, i)); }
		gramSchmidt_k(ortho_k, Ortho, k, k + 1, NumCols, First);
		for (size_t i = 0; i < NumCols; i++) { Ortho(k, i) = ortho_k[i]; }
	}
}

template<typename ContainerA, typename ContainerB, typename ContainerOut>
void gramSchmidt_allB_to_Ak(ContainerOut &Ortho, const ContainerA &A, const ContainerB &B, size_t k, size_t NumRows_B,
                            size_t NumCols, size_t First_B = 0) {
	// Assumes that A is 2-dimensional with NumCols columns
	//     and that B is 2-dimensional with NumRows_B x NumCols
	// Goal: make all rows of B orthogonal to the k'th row of A
	//        and all rows of B orthogonal to each other

	Ortho = B;
	std::vector<double> ortho_d(NumCols);
	for (size_t d = First_B; d < NumRows_B; d++) {
		// do Gram-Schmidt between k'th row of A and current row of B
		double sum = impl::dotProduct_jk(A, B, k, d, NumCols);
		for (size_t i = 0; i < NumCols; i++) { Ortho(d, i) -= sum * coretools::underlying(A(k, i)); }

		// copy
		for (size_t i = 0; i < NumCols; i++) { ortho_d[i] = Ortho(d, i); }

		// now do Gram-Schmidt between all rows of B
		gramSchmidt_k(ortho_d, Ortho, d, d + 1, NumCols, First_B);

		// save
		for (size_t i = 0; i < NumCols; i++) { Ortho(d, i) = ortho_d[i]; }
	}
}

template<typename ContainerA, typename ContainerB, bool Normalize = true>
void gramSchmidt_A_k_to_allB(std::vector<double> &A_k, const ContainerA &A, const ContainerB &B, size_t k,
                             size_t NumRows_B, size_t NumCols, size_t First_B = 0) {
	// Assumes that A is 2-dimensional with NumCols columns
	//     and that B is 2-dimensional with NumRows_B x NumCols
	//     and that B is an orthogonal matrix (all rows are orthogonal)
	// Goal: make the k'th row of A orthogonal to B
	// ATTENTION: assumes that A_k already contains the current values of the k'th row of A!
	assert(A_k.size() == NumCols);

	for (size_t j = First_B; j < NumRows_B; j++) {
		double sum = impl::dotProduct_jk(A, B, k, j, NumCols);
		for (size_t i = 0; i < NumCols; i++) { A_k[i] -= sum * coretools::underlying(B(j, i)); }
	}

	// normalize to unit length
	if constexpr (Normalize) { coretools::normalize(A_k, coretools::vectorNorm(A_k)); }
}

template<typename ContainerA, typename ContainerB>
void gramSchmidt_A_k_to_allA_and_allB(std::vector<double> &A_k, const ContainerA &A, const ContainerB &B, size_t k,
                                      size_t NumRows_A, size_t NumRows_B, size_t NumCols, size_t First_A = 0,
                                      size_t First_B = 0) {
	// Assumes that A is 2-dimensional with NumRows_A x NumCols
	//     and that B is 2-dimensional with NumRows_B x NumCols
	//     and that B is an orthogonal matrix (all rows are orthogonal)
	//     and that A is an orthogonal matrix except the k'th row
	// Goal: make the k'th row of A orthogonal to A and B
	// ATTENTION: assumes that A_k already contains the current values of the k'th row of A!
	assert(A_k.size() == NumCols);

	// make A_k orthogonal to B, don't normalize to unit length
	gramSchmidt_A_k_to_allB<ContainerA, ContainerB, false>(A_k, A, B, k, NumRows_B, NumCols, First_B);

	// make A_k orthogonal to all other A's
	gramSchmidt_k(A_k, A, k, NumRows_A, NumCols, First_A);
}

template<typename Container>
bool isOrthogonal(const Container &Vals, size_t NumRows, size_t NumCols, size_t First = 0) {
	for (size_t k = First; k < NumRows; k++) {
		for (size_t j = k + 1; j < NumRows; j++) {
			double sum = impl::dotProduct_jk(Vals, j, k, NumCols);
			if (std::fabs(sum) >= 1e-07) { return false; }
		}
	}
	return true;
}

template<typename ContainerA, typename ContainerB>
bool isOrthogonal_AB(const ContainerA &A, const ContainerB &B, size_t NumRows_A, size_t NumRows_B, size_t NumCols,
                     size_t First_A = 0, size_t First_B = 0) {
	// Checks if all A are orthogonal to all B
	for (size_t k = First_A; k < NumRows_A; k++) {
		for (size_t j = First_B; j < NumRows_B; j++) {
			double sum = impl::dotProduct_jk(A, B, k, j, NumCols);
			if (std::fabs(sum) >= 1e-07) { return false; }
		}
	}
	return true;
}

template<typename ContainerA, typename ContainerB>
bool isOrthogonal_A_B_AB(const ContainerA &A, const ContainerB &B, size_t NumRows_A, size_t NumRows_B, size_t NumCols,
                         size_t First_A = 0, size_t First_B = 0) {
	// Checks if A and B are both an orthogonal matrix (all rows are orthogonal)
	//    and if A is orthogonal to all B

	bool a  = isOrthogonal(A, NumRows_A, NumCols, First_A);
	bool b  = isOrthogonal(B, NumRows_B, NumCols, First_B);
	bool ab = isOrthogonal_AB(A, B, NumRows_A, NumRows_B, NumCols, First_A, First_B);
	return a && b && ab;
}

} // namespace GramSchmidt

#endif // BANGOLIN_TGRAMSCHMIDT_H
