//
// Created by madleina on 11.05.22.
//

#include "TIRLS.h"
#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "coretools/Main/TRandomGenerator.h"

using namespace testing;

TEST(TIRLSTest, IRLS_K1) {
	// simulate data
	size_t N = 100;
	size_t K = 1; // only intercept

	double mu = 0.7461;
	arma::mat X(N, K, arma::fill::ones);
	arma::vec y(N);
	for (size_t i = 0; i < N; i++) { y[i] = coretools::logistic(mu); }

	// now do IRLS
	const arma::mat Xt = X.t();
	arma::vec result = IRLS::doIRLS(y, X, Xt);

	EXPECT_EQ(result.size(), 1);
	EXPECT_NEAR(result[0], mu, 0.0001);
}

TEST(TIRLSTest, IRLS_K2) {
	// simulate data
	size_t N = 100;
	size_t K = 2;

	double mu = 0.7461;
	double v  = 2.73;
	arma::mat X(N, K, arma::fill::ones);
	for (size_t i = 0; i < N; i++) { X(i, 1) = coretools::instances::randomGenerator().getNormalRandom(0, 1); }
	arma::vec y(N);
	for (size_t i = 0; i < N; i++) { y[i] = coretools::logistic(mu + v * X(i, 1)); }

	// now do IRLS
	const arma::mat Xt = X.t();
	arma::vec result = IRLS::doIRLS(y, X, Xt);

	EXPECT_EQ(result.size(), 2);
	EXPECT_NEAR(result[0], mu, 0.0001);
	EXPECT_NEAR(result[1], v, 0.0001);
}

TEST(TIRLSTest, IRLS_K3) {
	// simulate data
	size_t N = 100;
	size_t K = 3;

	double mu   = 0.7461;
	arma::vec v = {2.73, -12.84};
	arma::mat X(N, K, arma::fill::ones);
	for (size_t i = 0; i < N; i++) {
		X(i, 1) = coretools::instances::randomGenerator().getNormalRandom(0, 1);
		X(i, 2) = coretools::instances::randomGenerator().getNormalRandom(0, 1);
	}
	arma::vec y(N);
	for (size_t i = 0; i < N; i++) { y[i] = coretools::logistic(mu + v[0] * X(i, 1) + v[1] * X(i, 2)); }

	// now do IRLS
	const arma::mat Xt = X.t();
	arma::vec result = IRLS::doIRLS(y, X, Xt);

	EXPECT_EQ(result.size(), 3);
	EXPECT_NEAR(result[0], mu, 0.0001);
	EXPECT_NEAR(result[1], v[0], 0.0001);
	EXPECT_NEAR(result[2], v[1], 0.0001);
}