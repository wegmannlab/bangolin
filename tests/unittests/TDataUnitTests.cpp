//
// Created by madleina on 12.04.21.
//

#include "BangolinTypes.h"
#include "TData.h"
#include "VCF/TSimulationVCF.h"
#include "gtest/gtest.h"
#include "gmock/gmock.h"

using namespace testing;

//--------------------------------------------
// TEnvironmentalDataReader
//--------------------------------------------

class BridgeTEnvironmentalDataReader : public TEnvironmentalDataReader {
public:
	void _fillTransposedObservationFromFile(coretools::TMultiDimensionalStorage<TypeUX, 2> &DT) {
		TEnvironmentalDataReader::_fillTransposedObservationFromFile(DT);
	}
};

class TEnvironmentalDataReaderTest : public Test {
public:
	BridgeTEnvironmentalDataReader reader;
	std::string filename = "env.txt";

	static void writeEnvFile(const std::string &FileName) {
		// header
		std::vector<std::string> header = {"-", "env1", "env2", "env3", "env4"};
		// rownames
		writeEnvFile(FileName, header);
	}

	static void writeEnvFile(const std::string &FileName, const std::vector<std::string> &Header) {
		// rownames
		std::vector<std::string> rownames = {"Indiv1", "Indiv2", "Indiv3", "Indiv4", "Indiv5"};
		writeEnvFile(FileName, Header, rownames);
	}

	static void writeEnvFile(const std::string &FileName, const std::vector<std::string> &Header,
	                         const std::vector<std::string> &Rownames) {
		coretools::TOutputFile file(FileName);

		// header
		file.writeHeader(Header);

		// data
		size_t numRows = Rownames.size();
		size_t numCols = Header.size() - 1;
		double value   = 0.5;
		for (size_t i = 0; i < numRows; i++) {
			// write rowname
			file << Rownames[i];
			for (size_t j = 0; j < numCols; j++) {
				file << value * value;
				value++;
			}
			file << std::endl;
		}

		file.close();
	}

	void SetUp() { coretools::instances::parameters().addParameter("env", filename); }
};

TEST_F(TEnvironmentalDataReaderTest, _fillTransposedObservationFromFile) {
	// write file
	writeEnvFile(filename);

	coretools::TMultiDimensionalStorage<TypeUX, 2> dt;
	reader._fillTransposedObservationFromFile(dt);

	EXPECT_EQ(reader.getNumIndividuals(), 5);
	EXPECT_EQ(reader.getNumEnvVar(), 4);

	auto indivNames = dt.getDimensionName(0);
	EXPECT_EQ(indivNames->size(), 5);
	EXPECT_EQ((*indivNames)[0], "Indiv1");
	EXPECT_EQ((*indivNames)[1], "Indiv2");
	EXPECT_EQ((*indivNames)[2], "Indiv3");
	EXPECT_EQ((*indivNames)[3], "Indiv4");
	EXPECT_EQ((*indivNames)[4], "Indiv5");

	auto envNames = dt.getDimensionName(1);
	EXPECT_EQ(envNames->size(), 4);
	EXPECT_EQ((*envNames)[0], "env1");
	EXPECT_EQ((*envNames)[1], "env2");
	EXPECT_EQ((*envNames)[2], "env3");
	EXPECT_EQ((*envNames)[3], "env4");

	double value = 0.5;
	for (size_t i = 0; i < 20; i++) {
		EXPECT_EQ(dt[i], value * value);
		value++;
	}

	remove(filename.c_str());
}

TEST_F(TEnvironmentalDataReaderTest, duplicateEnvNames) {
	// write file
	std::vector<std::string> header = {"", "env1", "env3", "env3", "env4"};
	writeEnvFile(filename, header);

	coretools::TMultiDimensionalStorage<TypeUX, 2> dt;
	EXPECT_THROW(
	    try { reader._fillTransposedObservationFromFile(dt); } catch (...) { throw std::runtime_error(""); },
	    std::runtime_error);

	remove(filename.c_str());
}

TEST_F(TEnvironmentalDataReaderTest, duplicateIndivNames) {
	// write file
	std::vector<std::string> header   = {"-", "env1", "env2", "env3", "env4"};
	std::vector<std::string> rownames = {"Indiv1", "Indiv2", "Indiv2", "Indiv4", "Indiv5"};
	writeEnvFile(filename, header, rownames);

	coretools::TMultiDimensionalStorage<TypeUX, 2> dt;
	EXPECT_THROW(
	    try { reader._fillTransposedObservationFromFile(dt); } catch (...) { throw std::runtime_error(""); },
	    std::runtime_error);

	remove(filename.c_str());
}

TEST_F(TEnvironmentalDataReaderTest, readFileAndFillObservation) {
	// write file
	writeEnvFile(filename);

	coretools::TMultiDimensionalStorage<TypeUX, 2> d;
	coretools::TRandomGenerator randomGenerator;
	reader.readFileAndFillObservation(d);

	EXPECT_EQ(reader.getNumIndividuals(), 5);
	EXPECT_EQ(reader.getNumEnvVar(), 4);

	// names should be transposed, too!
	auto envNames = d.getDimensionName(0);
	EXPECT_EQ(envNames->size(), 4);
	EXPECT_EQ((*envNames)[0], "env1");
	EXPECT_EQ((*envNames)[1], "env2");
	EXPECT_EQ((*envNames)[2], "env3");
	EXPECT_EQ((*envNames)[3], "env4");

	auto indivNames = d.getDimensionName(1);
	EXPECT_EQ(indivNames->size(), 5);
	EXPECT_EQ((*indivNames)[0], "Indiv1");
	EXPECT_EQ((*indivNames)[1], "Indiv2");
	EXPECT_EQ((*indivNames)[2], "Indiv3");
	EXPECT_EQ((*indivNames)[3], "Indiv4");
	EXPECT_EQ((*indivNames)[4], "Indiv5");

	// values should be 1) transposed and 2) standardized
	std::vector<double> values = {-0.465922886494408, -0.376322331399329, -0.143360888152125, 0.232961443247204,
	                              0.752644662798658,  -0.484501583111509, -0.371451213718824, -0.129200422163069,
	                              0.242250791555755,  0.742902427437647,  -0.499460217914541, -0.36725016023128,
	                              -0.11752005127401,  0.24973010895727,   0.73450032046256,   -0.51174100268263,
	                              -0.3636054492745,   -0.107734947933185, 0.255870501341315,  0.727210898549};
	for (size_t i = 0; i < 20; i++) { EXPECT_FLOAT_EQ(d[i], values[i]); }

	remove(filename.c_str());
}

//--------------------------------------------
// TGeneticDataReader
//--------------------------------------------

class TDataReaderTest : public Test {
protected:
	std::vector<uint16_t> phred_g1 = {0, 1, 0, 0, 3, 0, 1, 2, 0, 0, 2, 0, 1, 0, 0, 1, 0, 0, 1, 1, 1, 0, 0, 3, 1,
	                                  0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 1, 2, 3, 2, 0, 3, 0, 0, 1, 0, 0, 0, 1, 2, 0};
	std::vector<uint16_t> phred_g2 = {1, 2, 0, 0, 0, 1, 1, 0, 1, 1, 1, 0, 0, 2, 1, 2, 2, 0, 1, 1, 0, 2, 0, 0, 0,
	                                  0, 0, 0, 2, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1};
	std::vector<uint16_t> phred_g3 = {1, 0, 1, 3, 1, 2, 0, 1, 0, 1, 0, 0, 1, 0, 2, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0,
	                                  1, 3, 0, 0, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 1, 2, 3, 0, 0, 1, 0, 2, 1, 0, 1};
	std::vector<uint16_t> depth    = {14, 11, 9, 9, 6,  8, 12, 9,  10, 11, 5,  14, 7,  13, 13, 11, 11,
	                                  5,  9,  8, 9, 8,  6, 7,  10, 14, 10, 11, 13, 10, 7,  9,  11, 13,
	                                  8,  7,  9, 6, 10, 6, 15, 15, 10, 9,  11, 9,  6,  14, 9,  14};

public:
	std::string filename    = "geneticData.vcf.gz";
	std::string filenameEnv = "env.env";
	;

	size_t numLoci  = 10;
	size_t numIndiv = 5;
	size_t K        = 1;

	coretools::TMultiDimensionalStorage<TypeGTL, 2> observation;
	coretools::TMultiDimensionalStorage<TypeUX, 2> x;
	std::shared_ptr<genometools::TDistancesBinned<numDistGroupsType>> distances;

	void SetUp() override {
		coretools::instances::parameters().addParameter("vcf", filename);
		coretools::instances::parameters().addParameter("env", filenameEnv);
		coretools::instances::parameters().addParameter("minMAF", "0");
		distances = std::make_shared<genometools::TDistancesBinned<numDistGroupsType>>(1000000);
	}

	static double dePhred(double y) { return pow(10.0, -y / 10.0); }

	void writeToVcfFile(std::vector<std::string> &SampleNames) {
		// generate positions
		std::shared_ptr<genometools::TPositionsRaw> positions = std::make_shared<genometools::TPositionsRaw>();
		for (size_t l = 0; l < numLoci; ++l) { // loop over loci
			positions->add(l + 1, "1");
		}
		positions->finalizeFilling();

		// generate name classes
		auto individualNames = std::make_shared<coretools::TNamesStrings>(SampleNames);
		auto lociNames       = std::make_shared<genometools::TNamesPositions>(positions.get());

		// generate genetic data and fill it
		coretools::TMultiDimensionalStorage<TypeGTL, 2> geneticData;
		geneticData.prepareFillData(numLoci, {numIndiv});

		size_t linearIndex = 0;
		for (size_t l = 0; l < numLoci; ++l) {                     // loop over loci
			for (size_t i = 0; i < numIndiv; ++i, ++linearIndex) { // loop over individuals
				// write to vcf
				geneticData.emplace_back(genometools::PhredIntProbability(phred_g1[linearIndex]),
				                         genometools::PhredIntProbability(phred_g2[linearIndex]),
				                         genometools::PhredIntProbability(phred_g3[linearIndex]));
			}
		}
		geneticData.finalizeFillData();

		// now open vcf
		TVCFWriter vcf(lociNames, individualNames);
		vcf.openVCF(filename);

		// header
		vcf.writeHeader();

		linearIndex = 0;
		for (size_t l = 0; l < numLoci; ++l) { // loop over loci
			vcf.newSite();
			for (size_t i = 0; i < numIndiv; ++i, ++linearIndex) { // loop over individuals
				// write to vcf
				vcf.write(geneticData[linearIndex], depth[linearIndex]);
			}
		}
		vcf.closeVcf();
	}
};

TEST_F(TDataReaderTest, read_duplicateSampleNames) {
	std::vector<std::string> sampleNames = {"Indiv1", "Indiv2", "Indiv2", "Indiv4", "Indiv5"};
	writeToVcfFile(sampleNames);
	TEnvironmentalDataReaderTest::writeEnvFile(filenameEnv, {"-", "env1"}, sampleNames);

	EXPECT_ANY_THROW(Data::readData(x, observation, distances.get(), K));

	remove(filename.c_str());
	remove(filenameEnv.c_str());
}

TEST_F(TDataReaderTest, read_noShuffling) {
	std::vector<std::string> sampleNames = {"Indiv1", "Indiv2", "Indiv3", "Indiv4", "Indiv5"};
	writeToVcfFile(sampleNames);
	TEnvironmentalDataReaderTest::writeEnvFile(filenameEnv, {"-", "env1"}, sampleNames);

	Data::readData(x, observation, distances.get(), K);

	// check for dimensions
	EXPECT_EQ(observation.numDim(), 2);
	EXPECT_THAT(observation.dimensions(), ElementsAre(10, 5));

	// check for elements
	for (size_t il = 0; il < phred_g1.size(); il++) {
		EXPECT_FLOAT_EQ(coretools::Probability(observation[il][genometools::BiallelicGenotype::homoFirst]),
		                dePhred(phred_g1[il]));
		EXPECT_FLOAT_EQ(coretools::Probability(observation[il][genometools::BiallelicGenotype::het]),
		                dePhred(phred_g2[il]));
		EXPECT_FLOAT_EQ(coretools::Probability(observation[il][genometools::BiallelicGenotype::homoSecond]),
		                dePhred(phred_g3[il]));
	}

	// check for names
	auto lociNames = observation.getDimensionName(0);
	EXPECT_EQ(lociNames->size(), 10);
	EXPECT_EQ((*lociNames)[0], "1:1");
	EXPECT_EQ((*lociNames)[1], "1:2");
	EXPECT_EQ((*lociNames)[2], "1:3");
	EXPECT_EQ((*lociNames)[3], "1:4");
	EXPECT_EQ((*lociNames)[4], "1:5");
	EXPECT_EQ((*lociNames)[5], "1:6");
	EXPECT_EQ((*lociNames)[6], "1:7");
	EXPECT_EQ((*lociNames)[7], "1:8");
	EXPECT_EQ((*lociNames)[8], "1:9");
	EXPECT_EQ((*lociNames)[9], "1:10");

	auto indivNames = observation.getDimensionName(1);
	EXPECT_EQ(indivNames->size(), 5);
	EXPECT_EQ((*indivNames)[0], "Indiv1");
	EXPECT_EQ((*indivNames)[1], "Indiv2");
	EXPECT_EQ((*indivNames)[2], "Indiv3");
	EXPECT_EQ((*indivNames)[3], "Indiv4");
	EXPECT_EQ((*indivNames)[4], "Indiv5");

	indivNames = x.getDimensionName(1);
	EXPECT_EQ(indivNames->size(), 5);
	EXPECT_EQ((*indivNames)[0], "Indiv1");
	EXPECT_EQ((*indivNames)[1], "Indiv2");
	EXPECT_EQ((*indivNames)[2], "Indiv3");
	EXPECT_EQ((*indivNames)[3], "Indiv4");
	EXPECT_EQ((*indivNames)[4], "Indiv5");

	auto envNames = x.getDimensionName(0);
	EXPECT_EQ(envNames->size(), 1);
	EXPECT_EQ((*envNames)[0], "env1");

	remove(filename.c_str());
	remove(filenameEnv.c_str());
}

TEST_F(TDataReaderTest, read_ShuffleSkip) {
	std::vector<std::string> sampleNames = {"Indiv3", "Indiv1", "Indiv10", "Indiv4", "Indiv2"};
	writeToVcfFile(sampleNames);

	numIndiv                                = 4;
	std::vector<std::string> sampleNamesEnv = {"Indiv1", "Indiv2", "Indiv3", "Indiv4"};
	TEnvironmentalDataReaderTest::writeEnvFile(filenameEnv, {"-", "env1", "env2"}, sampleNamesEnv);

	Data::readData(x, observation, distances.get(), K);

	// check for dimensions
	EXPECT_EQ(observation.numDim(), 2);
	EXPECT_THAT(observation.dimensions(), ElementsAre(10, 4));

	// check for elements
	std::vector<uint16_t> phred_g1_shuffled = {1, 3, 0, 0, 1, 0, 0, 0, 0, 0, 2, 0, 0, 1, 1, 1, 0, 1, 1, 3,
	                                           0, 1, 0, 0, 0, 1, 1, 1, 2, 0, 1, 2, 0, 0, 3, 1, 0, 0, 0, 2};
	std::vector<uint16_t> phred_g2_shuffled = {2, 0, 1, 0, 1, 1, 1, 1, 0, 1, 1, 2, 2, 1, 2, 1, 2, 0, 0, 0,
	                                           0, 0, 0, 2, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0};
	std::vector<uint16_t> phred_g3_shuffled = {0, 1, 1, 3, 0, 1, 2, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	                                           3, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 3, 1, 2, 0, 2, 1, 0, 0};

	for (size_t il = 0; il < phred_g1_shuffled.size(); il++) {
		EXPECT_FLOAT_EQ(coretools::Probability(observation[il][genometools::BiallelicGenotype::homoFirst]),
		                dePhred(phred_g1_shuffled[il]));
		EXPECT_FLOAT_EQ(coretools::Probability(observation[il][genometools::BiallelicGenotype::het]),
		                dePhred(phred_g2_shuffled[il]));
		EXPECT_FLOAT_EQ(coretools::Probability(observation[il][genometools::BiallelicGenotype::homoSecond]),
		                dePhred(phred_g3_shuffled[il]));
	}

	// values should be 1) transposed and 2) standardized
	std::vector<double> values = {-0.526136791501647, -0.340441453324595, 0.0928476690885259, 0.773730575737716,
	                              -0.560475705652866, -0.316790616238577, 0.121842544707145,  0.755423777184298};
	for (size_t i = 0; i < 8; i++) { EXPECT_FLOAT_EQ(x[i], values[i]); }

	// check for names
	auto lociNames = observation.getDimensionName(0);
	EXPECT_EQ(lociNames->size(), 10);
	EXPECT_EQ((*lociNames)[0], "1:1");
	EXPECT_EQ((*lociNames)[1], "1:2");
	EXPECT_EQ((*lociNames)[2], "1:3");
	EXPECT_EQ((*lociNames)[3], "1:4");
	EXPECT_EQ((*lociNames)[4], "1:5");
	EXPECT_EQ((*lociNames)[5], "1:6");
	EXPECT_EQ((*lociNames)[6], "1:7");
	EXPECT_EQ((*lociNames)[7], "1:8");
	EXPECT_EQ((*lociNames)[8], "1:9");
	EXPECT_EQ((*lociNames)[9], "1:10");

	auto indivNames = observation.getDimensionName(1);
	EXPECT_EQ(indivNames->size(), 4);
	EXPECT_EQ((*indivNames)[0], "Indiv1");
	EXPECT_EQ((*indivNames)[1], "Indiv2");
	EXPECT_EQ((*indivNames)[2], "Indiv3");
	EXPECT_EQ((*indivNames)[3], "Indiv4");

	indivNames = x.getDimensionName(1);
	EXPECT_EQ(indivNames->size(), 4);
	EXPECT_EQ((*indivNames)[0], "Indiv1");
	EXPECT_EQ((*indivNames)[1], "Indiv2");
	EXPECT_EQ((*indivNames)[2], "Indiv3");
	EXPECT_EQ((*indivNames)[3], "Indiv4");

	auto envNames = x.getDimensionName(0);
	EXPECT_EQ(envNames->size(), 2);
	EXPECT_EQ((*envNames)[0], "env1");
	EXPECT_EQ((*envNames)[1], "env2");

	remove(filename.c_str());
	remove(filenameEnv.c_str());
}
