//
// Created by madleina on 31.03.21.
//

#include "TLogisticLookup.h"
#include "gtest/gtest.h"
#include "gmock/gmock.h"

using namespace testing;

TEST(TLogisticLookupTest, approxLogistic) {
	TLogisticLookup logisticLookup;

	uint64_t numPoints = 1000000;
	double start       = -20.;
	double end         = 20.;
	double step        = (end - start) / (double)numPoints;
	for (uint64_t i = 0; i < numPoints; i++) {
		double x = start + (double)i * step;
		EXPECT_NEAR(coretools::logistic(x), logisticLookup.approxLogistic(x), 2.8e-06);
	}
}
