/*
 * bangolin.cpp
 *
 *  Created on: Nov 21, 2018
 *      Author: caduffm
 */

#include "coretools/Main/TMain.h"
#include "coretools/Main/TTask.h"
#include "TBangolinCore.h"

void addTaks(coretools::TMain & main) {
    main.addRegularTask("simulate", new TTask_simulate());
	main.addRegularTask("infer", new TTask_infer());
}

void addTests(coretools::TMain &){}

//---------------------------------------------------------------------------
//Main function
//---------------------------------------------------------------------------
int main(int argc, char* argv[]) {
    coretools::TMain main("Bangolin", "2.0", "University of Fribourg","https://bitbucket.org/wegmannlab/bangolin", "madleina.caduff@unifr.ch");

	//add existing tasks
	addTaks(main);
    addTests(main);

	//now run program
	return main.run(argc, argv);
}


